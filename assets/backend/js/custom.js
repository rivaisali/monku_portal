// Login 
	$('#logForm').submit(function (e) {
		$('#responseDiv').hide();
		$('#username').removeClass("is-invalid");
		$('#password').removeClass("is-invalid");
		e.preventDefault();
		$('#logText').html('<i class="mdi mdi-loading mdi-spin"></i> Loading');
		const user = $('#logForm').serialize();
		const login = function () {
			$.ajax({
				type: 'POST',
				url: url + '/auth/login',
				dataType: 'json',
				data: user,
				success: function (response) {
					$('#responseDiv').html(response.message);
					$('#logText').html('Masuk');
					if (response.error) {
					$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
						$('#username').addClass("is-invalid");
						$('#password').addClass("is-invalid");
						
					} else {
						$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
					
						$('#username').removeClass("is-invalid");
						$('#password').removeClass("is-invalid");
						$('#username').addClass("is-valid");
						$('#password').addClass("is-valid");
						$('.toggle-password').hide();
						setTimeout(function () {
							location.reload();
						}, 3000);
					}
				}
			});
		};
		setTimeout(login, 3000);
	});

	//list features

	$('.feature').click(function(event){
		event.preventDefault();
		let result="";
		let package_id = $(this).data('id');
				$.ajax({
				url: url + '/ajax/list_features',
				type: 'post',
				dataType: 'json',
				data: {
					'package_id': package_id
				},
				success: function (results) {
					result +='<center><img class="irc_mut img-fluid" src="'+url+'/assets/frontend/images/'+results.data[0].image+'" width="80" height="80"></img></center><br>';
					result += '<table class="table table-hover table-borderless">';
					$.each(results.data, function(index, item) {
						result += '<tr align="center"><td width="20px"><i class="mdi '+item.icon+'"></i>'+'&nbsp;'+item.limits+'&nbsp;'+item.feature+'</td></tr>';
	
					});
					result += '</table>';
					$("#result").html(result);
					
				}
			})
		})

	//cek subdomain
	$("#domain").on('input', function () {
		let domain = $(this).val();
		const check = $("#check_domain");
		if (domain.length > 6) {
			check.show();
			check.html('<span class="btn btn-primary" style="font-size:12px;"><i class="mdi mdi-loading mdi-spin"></i>&nbsp; Tunggu Sebentar...</span>');
		
				$.ajax({
				url: url + '/ajax/check_domain',
				type: 'post',
				dataType: 'json',
				data: {
					'domain': domain
				},
				success: function (success) {
					check.html('<span class="'+success.label+'" style="font-size:12px;"><i class="mdi mdi-check"></i>&nbsp;'+success.msg+'</span>');
					if(success.status == true){
						$("#btn").hide();
					}else{
						$("#btn").show();
					}
				}
			})
			
		} else {
			check.hide();
		}
		})

  // mengganti alert notifikasi menjadi toast
const statusFlashData = $('.flash-data').data('statusflashdata');
const msgFlashData = $('.flash-data').data('msgflashdata');

// console.log(statusFlashData);
if (statusFlashData) {
	$.notify({
		// options
		title: '<strong>Information</strong>',
		message: '<br> '+msgFlashData,
		icon: 'mdi mdi-check',
	}, {
		// settings
		element: 'body',
		type: statusFlashData,
		showProgressbar: false,
		placement: {
			from: "top",
			align: "right"
		},
		delay: 3300,
		timer: 1000,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutRight'
		},
		icon_type: 'class',
	});
}

$(function() {
    function readBrideURL(input) {
        if(input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imageBride').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    };

	function readGroomURL(input) {
        if(input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
				$('#imageGroom').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#brideImage").change(function(){
        readBrideURL(this);
    });
    $("#imageBride").click(function() {
        $("input[id='brideImage']").click();
    });

	$("#groomImage").change(function(){
        readGroomURL(this);
    });
    $("#imageGroom").click(function() {
        $("input[id='groomImage']").click();
    });
});
