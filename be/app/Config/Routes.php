<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Frontend');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Frontend::index');
$routes->get('tema', 'Frontend::theme');
$routes->get('login', 'Auth::index');
$routes->get('daftar', 'Auth::signup');
$routes->get('aktivasi/(:segment)', 'Auth::activation/$1');
$routes->post('daftar', 'Auth::signup');

$routes->post('daftar', 'Auth::signup');
$routes->add('user/invitation/create', 'Invitation::start');
$routes->add('user/step/two/(:segment)/(:segment)', 'Invitation::step_two/$1/$2');
$routes->add('user/step/three/(:segment)/(:segment)', 'Invitation::step_three/$1/$2');
$routes->add('user/step/four/(:segment)/(:segment)', 'Invitation::step_four/$1/$2');
$routes->add('user/payment/(:segment)', 'Invitation::payment/$1');

$routes->add('payment/pay/(:segment)', 'Payment::index/$1');
$routes->add('payment/notification/handling', 'Payment::notification');
$routes->add('payment/finish', 'Frontend::payment_finish');

$routes->add('user/start', 'User::start');

$routes->group('/', ['filter' => 'user'], function ($routes) {
    $routes->add('user/', 'User::index');
    $routes->add('user/settings', 'User::setting');
    //events
    $routes->add('user/event', 'Event::index');
    $routes->add('user/event/create', 'Event::create');
    $routes->add('user/event/(:segment)', 'Event::update/$1');
    $routes->add('user/event/delete/(:segment)', 'Event::delete/$1');
    //guests
    $routes->add('user/guest', 'Guest::index');
    $routes->add('user/send', 'Guest::send');
    $routes->add('user/guest/create', 'Guest::create');
    $routes->add('user/guest/(:segment)', 'Guest::update/$1');
    $routes->add('user/guest/delete/(:segment)', 'Guest::delete/$1');
    //groups
    $routes->add('user/group', 'Group::index');
    $routes->add('user/group/create', 'Group::create');
    $routes->add('user/group/(:segment)', 'Group::update/$1');
    $routes->add('user/group/delete/(:segment)', 'Group::delete/$1');
    //bridegroom
    $routes->add('user/bridegroom', 'Bridegroom::index');
   //gallery
   $routes->add('user/gallery', 'Gallery::index');
   $routes->add('user/gallery/create', 'Gallery::create');
   //template message
   $routes->add('user/message/template', 'Invitation::create_template_message');
});



//admin routes
$routes->group('/', ['filter' => 'admin'], function ($routes) {
$routes->add('admin/', 'Admin::index');
//package
$routes->add('admin/package', 'Package::index');
$routes->add('admin/package/detail/(:segment)', 'Package::detail/$1');
$routes->add('admin/package/create', 'Package::create');
$routes->add('admin/package/(:segment)', 'Package::update/$1');
$routes->add('admin/package/delete/(:segment)', 'Package::delete/$1');

$routes->add('admin/package/detail/(:segment)', 'Package::detail/$1');
$routes->add('admin/package/detail/(:segment)/create', 'Package::detail_create/$1');
$routes->add('admin/package/detail/(:segment)/edit', 'Package::detail_update/$1');
$routes->add('admin/package/detail/(:segment)/delete', 'Package::detail_delete/$1');


//feature
$routes->add('admin/feature', 'Feature::index');
$routes->add('admin/feature/create', 'Feature::create');
$routes->add('admin/feature/(:segment)', 'Feature::update/$1');
$routes->add('admin/feature/delete/(:segment)', 'Feature::delete/$1');

});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
