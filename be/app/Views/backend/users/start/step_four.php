<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<style>
    .cursor-pointer {
        cursor: pointer;
        color: #42A5F5
    }

    .pic {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .card-block {
        width: 200px;
        border: 1px solid lightgrey;
        border-radius: 5px !important;
        background-color: #FAFAFA;
        margin-bottom: 15px;
        font-size: 10px;
    }

    .card-body.show {
        display: block
    }

    .card {
        padding-bottom: 20px;
        box-shadow: 2px 2px 6px 0px rgb(200, 167, 216)
    }

    .radio {
        display: inline-block;
        border-radius: 0;
        box-sizing: border-box;
        cursor: pointer;
        color: #000;
        font-weight: 100;
        -webkit-filter: grayscale(100%);
        -moz-filter: grayscale(100%);
        -o-filter: grayscale(100%);
        -ms-filter: grayscale(100%);
        filter: grayscale(100%)
    }

    .radio:hover {
        box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.1)
    }

    .radio.selected {
        box-shadow: 0px 8px 16px 0px #EEEEEE;
        -webkit-filter: grayscale(0%);
        -moz-filter: grayscale(0%);
        -o-filter: grayscale(0%);
        -ms-filter: grayscale(0%);
        filter: grayscale(0%)
    }

    .selected {
        background-color: #E0F2F1
    }

    .a {
        justify-content: center !important
    }
</style>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="col-lg-6 mt-4 offset-md-3">
            <div class="card border-0 rounded shadow p-4">
                <h3 class="mb-0 text-center"><strong>Pilih Tema dan Desain</strong></h3>
                <span class="text-muted text-center">Berbagai pilihan tema sudah kami sediakan. Mana yang kamu
                    suka?</span>
                    <form method="POST" action="">
                <?=csrf_field()?>
                <div class="col-lg-12">
                    <div class="mb-3 mt-4">
                        <div class="form-floating mb-2">
                        <div class="radio-group row justify-content-between px-3 a">
                       <?php foreach($theme as $d) :?>
                        <div class="col-auto mr-sm-2 mx-1 card-block py-0 text-center radio">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="<?=base_url()?>/uploads/themes/<?=$d->image?>" width="100"> </div>
                                     <p class="text badge bg-primary"><a href="<?=$d->demo_link?>" target="_blank">Lihat Demo</a></p>
                                     <input type="radio" hidden name="theme_id" value="<?=$d->id?>">    
                                </div>
                            </div>
                        </div>
                       
                    <?php endforeach; ?>
                    </div>
                    <div class="invalid-feedback d-block"><?=$validation->getError("theme_id");?></div>
                                </div>
                            </div>
                        </div>
                        
                                <div class="col-lg-12 mt-2 mb-0" align="right">
                                    <button type="submit" name="submit" value="start"
                                        class="btn btn-primary">Selanjutnya</button>
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                            </form>
                        </div>
                    </div>
                    <!--end col-->
                </div>
            </div>
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.radio-group .radio').click(function () {
                    $('.radio').removeClass('selected');
                    $(this).addClass('selected');
                    $('.selected input[name=theme_id]').prop('checked', true);
                });
            });



            document.getElementById("close-sidebar").style.display = "none";
            document.getElementById("sidebar").style.display = "none";
            document.getElementsByClassName("page-wrapper")[0].classList.toggle("toggled");
        </script>
        <?=$this->endSection();?>