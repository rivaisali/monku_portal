<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<style>
    .cursor-pointer {
        cursor: pointer;
        color: #42A5F5
    }

    .pic {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .card-block {
        width: 100%;
        border: 1px solid lightgrey;
        border-radius: 5px !important;
        background-color: #FAFAFA;
        margin-bottom: 15px;
        font-size: 10px;
    }

    .card-body.show {
        display: block
    }

    .card {
        padding-bottom: 20px;
        box-shadow: 2px 2px 6px 0px rgb(200, 167, 216)
    }

    .radio {
        display: inline-block;
        border-radius: 0;
        box-sizing: border-box;
        cursor: pointer;
        color: #000;
        font-weight: 100;
        -webkit-filter: grayscale(100%);
        -moz-filter: grayscale(100%);
        -o-filter: grayscale(100%);
        -ms-filter: grayscale(100%);
        filter: grayscale(100%)
    }

    .radio:hover {
        box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.1)
    }

    .radio.selected {
        box-shadow: 0px 8px 16px 0px #EEEEEE;
        -webkit-filter: grayscale(0%);
        -moz-filter: grayscale(0%);
        -o-filter: grayscale(0%);
        -ms-filter: grayscale(0%);
        filter: grayscale(0%)
    }

    .selected {
        background-color: #E0F2F1
    }

    .a {
        justify-content: center !important
    }
</style>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="col-lg-6 mt-4 offset-md-3">
            <div class="card border-0 rounded shadow p-4">
                <h3 class="mb-0 text-center"><strong>Pilih Paket</strong></h3>
                <span class="text-muted text-center">Kami menawarkan paket dengan harga spesial</span>
                <form method="POST" action="">
                <?=csrf_field()?>
                <div class="col-lg-12">
                    <div class="mb-3 mt-4">
                        <div class="form-floating mb-2">
                            <div class="radio-group row justify-content-between px-3 text-center a">
                                <?php foreach($package as $d) : ?>

                                <div class="mr-sm-2 mx-1 card-block py-0 radio">
                                    <div class="row">
                                        <div class="col">
                                        
                                            <div class="d-flex align-items-center mt-2">
                                                
                                                <div class="pic"> <img class="irc_mut img-fluid"
                                                        src="<?=base_url()?>/assets/frontend/images/<?=$d->image?>"
                                                        width="60" height="60"> </div>
                                                <div class="flex-1 ms-1" style="text-align:left;">
                                                    <h6 class="mb-0"><?=$d->package?></h6>
                                                    <?php foreach($feature as $p) : ?>
                                                       <?php if($p->package_id === $d->id):?>
                                                    <small class="text" style="text-align:left;"><i class="mdi <?=$p->icon?>"></i> <?=$p->quota?></small>&nbsp;
                                                    <?php  endif; endforeach; ?>
                                                    <div class="mt-1">
                                                    <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#featuremodal" data-id="<?=$d->id?>" class="badge bg-soft-primary feature">Detail Paket</a>
                                                    <input type="radio" name="package_id" checked value="<?=$d->id?>">    
                                                </div>
                                                    <br>
                                                </div>
                                                 
                                            </div>
                                        </div>
                                        <div class="col mt-2">
                                            <div class="d-flex align-items-center">
                                            <div class="flex-1 ms-4 mt-2" style="text-align:right;">
                                            <?php $discount = $d->price * $d->discount / 100; ?>
                                                    <span class="mb-0 text-danger"><del>Rp <?=rupiah($d->price)?></del></span>
                                                    <h6 class="mb-0">Rp <?=rupiah($d->price-$discount)?></h6>
                                                   </div>   
                                                  
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php endforeach; ?>

                            </div>
                            <div class="invalid-feedback d-block"><?=$validation->getError("user_type");?></div>
                        </div>
                    </div>
                </div>

                          
                <div class="col-lg-12 mt-2 mb-0" align="right">
                    <button type="submit" name="submit" value="start" class="btn btn-primary">Selanjutnya</button>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
            </form>
        </div>
    </div>
    <!--end col-->
</div>
</div>
 <!-- Start Modal -->
    <div class="modal fade" id="featuremodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header border-bottom p-3">
                        <h5 class="modal-title" id="exampleModalLabel">Fitur</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body p-3 pt-4">
                        <div class="row">
                            <div class="col-md-12 mt-4 mt-sm-0">
                                <div class="ms-md-4">
                                        <div class="row">  
                                            <div id="result"></div>
                                            </div><!--end col-->
                                        </div>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div>
                </div>
            </div>
        </div>
        <!-- End modal -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.radio-group .radio').click(function () {
            $('.radio').removeClass('selected');
            $(this).addClass('selected');
            $('.selected input[name=package_id]').prop('checked', true);
        });
    });

  

    document.getElementById("close-sidebar").style.display = "none";
    document.getElementById("sidebar").style.display = "none";
    document.getElementsByClassName("page-wrapper")[0].classList.toggle("toggled");
</script>
<?=$this->endSection();?>