<?=$this->extend('backend/main');?>
<?=$this->section('content');?>

<div class="container-fluid">
    <div class="layout-specing">
        <div class="col-lg-6 mt-4 offset-md-3">
            <div class="card border-0 rounded shadow p-4">
                <h3 class="mb-0 text-center"><strong>Siapkan Domain Anda</strong></h3>
                <span class="text-muted text-center">Pilih alamat yang diinginkan dan pastinya juga mudah
                    diingat.</span>
                    <form method="POST" action="">
                <?=csrf_field()?>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <div class="p-4">
                            <div class="mb-3">
                                <label for="subdomain">Alamat Website</label>
                                <div class="mt-2">
                                <div class="input-group">
                                    <input type="text" style="text-align: right;" class="form-control" style="padding: 10px;"
                                        placeholder="Subdomain" name="domain" id="domain" 
                                        aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <span class="input-group-text" style="padding: 11px;"
                                            id="basic-addon2">.monku.id</span>
                                    </div>
                                </div>
                                </div>
                                
                                <small class="text-muted mt-0">Buat nama domain yang unik.</small>
                            </div>
                            <div id="check_domain" style="display: none;"></div>
                        </div>
                        
                          </div>
                    <div class="col-lg-12 mt-2 mb-0" align="right">
                        <button type="submit" id="btn" name="submit" value="next" class="btn btn-primary">Selanjutnya</button>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
                </form>
            </div>
        </div>
        <!--end col-->
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
       $("#btn").hide();
    });

    document.getElementById("close-sidebar").style.display = "none";
    document.getElementById("sidebar").style.display = "none";
    document.getElementsByClassName("page-wrapper")[0].classList.toggle("toggled");
</script>
<?=$this->endSection();?>