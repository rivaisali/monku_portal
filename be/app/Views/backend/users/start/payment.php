<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">
        
        <div class="col-lg-6 mt-4 offset-md-3">
        <center><div class="mb-4 p-2 badge bg-success text-center">
            Untuk aktivasi akun Anda harap melakukan pembayaran dengan metode yang tersedia</div></center>
       
            <div class="card border-0 rounded shadow p-4">
                <h3 class="mb-0 text-center"><strong><?=$data->title?></strong></h3>
                <span class="text-muted text-center mt-2"><b>INVOICE #<?=$data->transaction_number?></b> · <?=$data->transaction_date?></span>
                    <form method="POST" action="">
                <?=csrf_field()?>
                        <div class="row mt-4">
                            <div class="col">
                                <div class="card shadow rounded border-0">
                                    <div class="card-body">
                                       
                                        <div class="invoice-middle py-4">
                                           
                                            <div class="row mb-0">
                                                <div class="col-xl-9 col-md-8 order-2 order-md-1">
                                                    <dl class="row">
                                                        <dt class="col-md-3 col-5 fw-normal">No Invoice. :</dt>
                                                        <dd class="col-md-9 col-7 text-muted"><?=$data->transaction_number?></dd>
                                                        <dt class="col-md-3 col-5 fw-normal">Nama :</dt>
                                                        <dd class="col-md-9 col-7 text-muted"><?=$data->full_name?></dd>
                                                        <dt class="col-md-3 col-5 fw-normal">Telpon :</dt>
                                                        <dd class="col-md-9 col-7 text-muted"><?=$data->phone?></dd>
                                                    </dl>
                                                </div>
            
                                            </div>
                                        </div>
            
                                        <div class="invoice-table pb-4">
                                            <div class="table-responsive bg-white shadow rounded">
                                                <table class="table mb-0 table-center invoice-tb">
                                                    <thead class="bg-light">
                                                        <tr>
                                                            <th scope="col" class="border-bottom text-start">No.</th>
                                                            <th scope="col" class="border-bottom text-start">Item</th>
                                                            <th scope="col" class="border-bottom">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row" class="text-start">1</th>
                                                            <td class="text-start"><?=$data->package?> (<?=$data->full_domain?>)</td>
                                                            <td>Rp <?=rupiah($data->total)?></td>
                                                        </tr>
                                                       
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
        
                                            <div class="row">
                                                <div class="col-lg-4 col-md-5 ms-auto">
                                                    <ul class="list-unstyled h6 fw-normal mt-4 mb-0 ms-md-5 ms-lg-4">
                                                        <li class="text-muted d-flex justify-content-between">Subtotal :<span> <?=rupiah($data->total)?></span></li>
                                                        <li class="text-muted d-flex justify-content-between">Pajak :<span> 0</span></li>
                                                        <li class="d-flex justify-content-between">Total :<span><?=rupiah($data->total)?></span></li>
                                                    </ul>
                                                </div><!--end col-->
                                            </div><!--end row-->
                                        </div>
            
                                        <div class="invoice-footer border-top pt-4">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="text-sm-start text-muted text-center">
                                                        <h6 class="mb-0">Whatsapp Monku Indonesia : <a href="https://wa.me/62895340111460" class="text-warning">(+62) 895 34011 1460</a></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 text-end">
                                <a href="<?=base_url()?>/payment/pay/<?=encrypt($data->invitation_id)?>" class="btn btn-primary"><i class="ti ti-credit-card"></i> Bayar Sekarang</a>
                                  
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div>
                    </div>
                </div><!--end container-->
                <script type="text/javascript">
    document.getElementById("close-sidebar").style.display = "none";
    document.getElementById("sidebar").style.display = "none";
    document.getElementsByClassName("page-wrapper")[0].classList.toggle("toggled");
</script>
<?=$this->endSection();?>