<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<style>
.cursor-pointer {
    cursor: pointer;
    color: #42A5F5
}

.pic {
    margin-top: 5px;
    margin-bottom: 10px;
}

.card-block {
     width: 120px; 
    border: 1px solid lightgrey;
    border-radius: 5px !important;
    background-color: #FAFAFA;
    margin-bottom: 10px;
    font-size:11px;
}

.card-body.show {
    display: block
}

.card {
    padding-bottom: 8px;
    box-shadow: 2px 2px 6px 0px rgb(200, 167, 216)
}

.radio {
    display: inline-block;
    border-radius: 0;
    box-sizing: border-box;
    cursor: pointer;
    color: #000;
    font-weight: 100;
    -webkit-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    filter: grayscale(100%)
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.1)
}

.radio.selected {
    box-shadow: 0px 8px 16px 0px #EEEEEE;
    -webkit-filter: grayscale(0%);
    -moz-filter: grayscale(0%);
    -o-filter: grayscale(0%);
    -ms-filter: grayscale(0%);
    filter: grayscale(0%)
}

.selected {
    background-color: #E0F2F1
}

.a {
    justify-content: center !important
}

</style>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="col-lg-6 mt-4 offset-md-3">
            <div class="card border-0 rounded shadow p-4">
                <h3 class="mb-0 text-center"><strong>Selamat Datang</strong></h3>
                <span class="text-muted text-center">Ceritakan pernikahanmu untuk membuat undangan yang unik dan
                    berkesan.</span>
                    <?php echo form_open();?> 
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <div class="form-floating mb-2">
                                    <input type="text" name="title" class="form-control <?=($validation->hasError('title')) ? 'is-invalid' : '';?>" placeholder="Judul">
                                    <label>Judul</label>
                                    <small class="text-muted">contoh : Ida & Adi</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <div class="form-floating mb-2">
                                    <input type="text" class="form-control <?=($validation->hasError('date')) ? 'is-invalid' : '';?>" name="date" placeholder="Tanggal Pernikahan">
                                    <label>Tanggal Pernikahan</label>
                                    <small class="text-muted">Kosongi jika masih belum ditentukan</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label>Sebagai</label>
                                <div class="form-floating mb-2">
                                <div class="radio-group row justify-content-between px-3 text-center a">
                        <div class="col-auto mr-sm-2 mx-1 card-block py-0 text-center radio selected">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="<?=base_url()?>/assets/backend/images/client/groom_placeholder.png" width="46" height="46"> </div>
                                    <p class="text">Calon Pengantin Pria</p>
                                    <input type="radio" name="user_type" checked value="1" style="opacity:0">
                                </div>
                            </div>
                        </div>
                        <div class="col-auto mr-sm-2 mx-1 card-block py-0 text-center radio">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="<?=base_url()?>/assets/backend/images/client/bride_placeholder.png" width="46" height="46"> </div>
                                    <input type="radio" name="user_type" value="2" style="opacity:0">
                                    <p class="text">Calon Pengantin Wanita</p>
                                </div>
                            </div>
                        </div> <div class="col-auto mr-sm-2 mx-1 card-block py-0 text-center radio">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="<?=base_url()?>/assets/backend/images/client/family_placeholder.png" width="46" height="46"> </div>
                                </div>
                                <div class="col">
                                <input type="radio" name="user_type" value="3" style="opacity:0">
                                    <p class="text">Keluarga</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto ml-sm-2 mx-1 card-block py-0 text-center radio">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="<?=base_url()?>/assets/backend/images/client/user_placeholder.png" width="46" height="46"> </div>
                                    <input type="radio" name="user_type" value="4" style="opacity:0"> 
                                    <p class="text">Panitia</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="invalid-feedback d-block"><?=$validation->getError("user_type");?></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 mt-2 mb-0" align="right">
                            <button type="submit" name="submit" value="start" class="btn btn-primary">Selanjutnya</button>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </form>
            </div>
        </div>
        <!--end col-->
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
        $('.radio-group .radio').click(function () {
        $('.radio').removeClass('selected');
        $(this).addClass('selected');
        $('.selected input[name=type_user]').prop('checked', true);
         });
    });

        document.getElementById("close-sidebar").style.display = "none";
        document.getElementById("sidebar").style.display = "none";
        document.getElementsByClassName("page-wrapper")[0].classList.toggle("toggled");
    </script>
<?=$this->endSection();?>