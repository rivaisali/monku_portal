<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-flex align-items-center justify-content-between">
            <div>
                <h6 class="text-muted mb-1">Selamat datang, <?=user("full_name");?>!</h6>
                <h5 class="mb-0">Dashboard</h5>
            </div>
        </div>

        <div class="row row-cols-xl-4 row-cols-md-2 row-cols-1">
            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <i class="uil uil-users-alt fs-4 mb-0"></i>
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Grup tamu</h6>
                            <p class="fs-5 text-dark fw-bold mb-0"><?=$count_group->count?></p>
                        </div>
                    </div>
                </a>
            </div>
            <!--end col-->

            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <i class="uil uil-user-circle fs-4 mb-0"></i>
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Tamu</h6>
                            <p class="fs-5 text-dark fw-bold mb-0"><?=$count_guest->count?></p>
                        </div>
                    </div>
                </a>
            </div>
            <!--end col-->

            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <i class="uil uil-shopping-bag fs-4 mb-0"></i>
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Penerima Tamu</h6>
                            <p class="fs-5 text-dark fw-bold mb-0"><?=$count_guest->count?></p>
                        </div>
                    </div>
                </a>
            </div>
            <!--end col-->

            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <i class="uil uil-store fs-4 mb-0"></i>
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Acara</h6>
                            <p class="fs-5 text-dark fw-bold mb-0"><?=$count_event->count?></p>
                        </div>
                    </div>
                </a>
            </div>
            <!--end col-->


        </div>
        <!--end row-->



        <div class="row">
            <div class="col-xl-3 col-lg-3 rounded">
                <div class="row">
                    <div class="col-xl-12 mt-4">
                        <div class="col mt-4">
                            <div class="card border-0 work-container work-classic rounded overflow-hidden">
                                <div class="card-body p-0">
                                    <a href="assets/images/work/1.jpg" class="lightbox d-inline-block" title="">
                                        <?php if($data->groom_photo === null) { ?>
                                        <img src="<?=base_url()?>/assets/backend/images/default_groom.png"
                                            class="img-fluid" alt="work-image">
                                            <?php }else{ ?>
                                                <img src="<?=base_url()?>/uploads/bridegroom/<?=$data->groom_photo?>"
                                            class="img-fluid" alt="work-image">
                                                <?php } ?>
                                    </a>
                                    <div class="content p-3">
                                        <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark title"><?=$data->groom_name?></a>
                                        </h5>
                                        <small class="text-muted"><?=$data->groom_about?></small>
                                        <h6 class="text-muted tag mb-0">Pengantin Pria</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
            </div>
            <div class="col-xl-3 col-lg-3 rounded">
                <div class="row">
                    <div class="col-xl-12 mt-4">
                        <div class="col mt-4">
                            <div class="card border-0 work-container work-classic rounded overflow-hidden">
                                <div class="card-body p-0">
                                    <a href="assets/images/work/1.jpg" class="lightbox d-inline-block" title="">
                                    <?php if($data->groom_photo === null) { ?>
                                        <img src="<?=base_url()?>/assets/backend/images/default_bride.png"
                                            class="img-fluid" alt="work-image">
                                            <?php }else{ ?>
                                                <img src="<?=base_url()?>/uploads/bridegroom/<?=$data->bride_photo?>"
                                            class="img-fluid" alt="work-image">
                                                <?php } ?>
                                    </a>
                                    <div class="content p-3">
                                        <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark title"><?=$data->bride_name?></a>
                                        </h5>
                                        <small class="text-muted"><?=$data->bride_about?></small>
                                        <h6 class="text-muted tag mb-0">Pengantin Wanita</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
            </div>
            <div class="col-xl-6 col-lg-6 mt-4 rounded">
                <div class="card shadow border-0">
                    <div class="p-4 border-bottom">
                        <div class="d-flex justify-content-between">
                            <h6 class="mb-0 fw-bold">Detail Undangan</h6>

                            <a href="#!" class="text-primary">Lihat Undangan <i
                                    class="uil uil-arrow-right align-middle"></i></a>
                        </div>
                    </div>

                    <div class="table-responsive shadow rounded-bottom" data-simplebar>
                        <table class="table table-center bg-white mb-0">
                            <tr>
                                <td class="p-3">Judul Undangan</td>
                                <td>:</td>
                                <td><?=$data->title?></td>
                            </tr>
                            <tr>
                                <td class="p-3">Domain</td>
                                <td>:</td>
                                <td><?=$data->full_domain?></td>
                            </tr>
                            <tr>
                                <td class="p-3">Paket</td>
                                <td>:</td>
                                <td><?=$data->package?></td>
                            </tr>
                            <tr>
                                <td class="p-3">Harga</td>
                                <td>:</td>
                                <td><?=rupiah($data->price)?></td>
                            </tr>
                            <tr>
                                <td class="p-3">Status</td>
                                <td>:</td>
                                <td>
                                    <?php
                                                $status = $data->invitation_status;
                                                $status_trs = $data->transaction_status;
                                                if($status==="0") echo'<span class="badge bg-danger">Belum Aktif</span><br>';
                                                else if($status==="1") echo'<span class="badge bg-success">Aktif</span><br>';
                                                else echo'<span class="badge bg-warning">Belum Lengkap</span>';

                                                if($status_trs==="0") echo'<small class="text-muted">Anda belum melakukan pembayaran</small>';
                                                
                                                ?>
                                </td>
                            </tr>

                        </table>
                    
                    </div>
                    
                    <!--end col-->
                    <!--end row-->
                </div>
                <!--end col-->
              
                
            </div>
             <!--end row-->
        </div>
    </div>
    <!--end container-->
    <?=$this->endSection();?>