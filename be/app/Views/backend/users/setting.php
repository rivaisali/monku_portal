<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">

        <div class="row">
            <div class="col-md-12 mt-4">
                <div class="card border-0 rounded shadow p-4">
                    <div class="row">

                        <div class="col-md-6">
                            <h4><?=$data->title?></h4>
                        </div>
                        <div class="col-md-6"> 
                            <div class="table-responsive shadow rounded-bottom" data-simplebar>
                                <table class="table table-center bg-white mb-0">
                                    <tr>
                                        <td class="p-3">Paket & Tambahan</td>
                                        <td>:</td>
                                        <td><?=$data->package?></td>
                                    </tr>
                                    <tr>
                                        <td class="p-3">Tagihan</td>
                                        <td>:</td>
                                        <td><?=$data->price?></td>
                                    </tr>
                                    <tr>
                                        <td class="p-3">Domain</td>
                                        <td>:</td>
                                        <td><?=$data->full_domain?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-4">
            <div class="row row-cols-xl-4 row-cols-md-2 row-cols-1">
            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                        <img src="<?php echo base_url()?>/assets/backend/images/icon/theme.png" height="64" whidth="64">
                     
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Ubah Tema</h6>
                          
                        </div>
                    </div>
                </a>
            </div>
            <div class="col mt-4">
                <a href="<?=base_url("user/event")?>"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                        <img src="<?php echo base_url()?>/assets/backend/images/icon/events.png" height="64" whidth="64">
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Rangkaian Acara</h6>
                          
                        </div>
                    </div>
                </a>
            </div>
            <div class="col mt-4">
                <a href="<?=base_url("user/bridegroom")?>"
                    class="features feature-success d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <img src="<?php echo base_url()?>/assets/backend/images/icon/love.png" height="64" whidth="64">
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Calon Pengantin</h6>
                          
                        </div>
                    </div>
                </a>
            </div>
            <div class="col mt-4">
                <a href="<?=base_url("user/gallery")?>"
                    class="features feature-success d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                        <img src="<?php echo base_url()?>/assets/backend/images/icon/gallery.png" height="64" whidth="64">
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Galeri</h6>
                          
                        </div>
                    </div>
                </a>
            </div>
            <div class="col mt-4">
                <a href="<?=base_url("user/message/template")?>"
                    class="features feature-success d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                        <img src="<?php echo base_url()?>/assets/backend/images/icon/comment.png" height="64" whidth="64">
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Template Pesan</h6>
                          
                        </div>
                    </div>
                </a>
            </div>
            <div class="col mt-4">
                <a href="#!"
                    class="features feature-success d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                        <img src="<?php echo base_url()?>/assets/backend/images/icon/wallet.png" height="64" whidth="64">
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Dompet Digital</h6>
                          
                        </div>
                    </div>
                </a>
            </div>
            </div>
            </div>
            <!--end col-->


        </div>
        <!--end row-->
    </div>
</div>
<!--end container-->
<?=$this->endSection();?>