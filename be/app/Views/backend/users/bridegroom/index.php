<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<style>
    .uploader {
        width: 200px;
        height: 200px;
        cursor: pointer;
        padding:5px;
        /* dashed border */
        background-image: url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='10' ry='10' stroke='black' stroke-width='4' stroke-dasharray='1%2c6' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e");
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/settings")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-6 mt-4">
                <div class="col-lg-12 mt-4">
                    <div class="card border-0 rounded shadow p-4">
                    <?php echo form_open_multipart("user/bridegroom");?>
                        <div class="row mt-4">
                            <div class="col-lg-12">
                            <div class="mb-3">
                                <label class="form-label">Calon Pengantin Pria</label>
                                        <div>
                                            <?php
                                            if($data->groom_photo===null || $data->groom_photo===""){
                                                $img =base_url()."/assets/backend/images/icon/plus.png";
                                            }else{
                                                $img =base_url()."/uploads/bridegroom/".$data->groom_photo;
                                            }
                                            ?>
                                            <img id="imageGroom" class="uploader mt-2" alt="upload" src="<?=$img?>">
                                            <input type="file" id="groomImage" style="display:none;" name="imageGroom" value="<?=$data->groom_photo?>">
                                        </div>
                                        <small class="text-muted">Klik + dikotak untuk mengubah</small>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="groom_name"
                                            class="form-control <?=($validation->hasError('groom_name')) ? 'is-invalid' : '';?>"
                                            placeholder="Nama Calon Pengantin Pria" value="<?=set_value('groom_name', $data->groom_name); ?>">
                                        <label>Nama Calon Pengantin Pria</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="groom_nickname"
                                            class="form-control <?=($validation->hasError('groom_nickname')) ? 'is-invalid' : '';?>"
                                            placeholder="Nama Panggilan Pengantin Pria" value="<?=set_value('groom_nickname', $data->groom_nickname); ?>">
                                        <label>Nama Panggilan Pengantin Pria</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="groom_about"
                                            class="form-control <?=($validation->hasError('groom_about')) ? 'is-invalid' : '';?>"
                                            placeholder="Deskripsi/Bio" value="<?=set_value('groom_about', $data->groom_about); ?>">
                                        <label>Deskripsi/Bio</label>
                                        <small class="text-muted">Contoh : Anak pertama dari bpk.samsi</small>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="groom_ig"
                                            class="form-control <?=($validation->hasError('groom_ig')) ? 'is-invalid' : '';?>"
                                            placeholder="Username Instagram" value="<?=set_value('groom_ig', $data->groom_ig); ?>">
                                        <label>Username Instagram</label>
                                        <small class="text-muted">Contoh : @monkuid</small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-2 mb-0" align="right">
                                <button type="submit" name="submit" value="save_groom" class="btn btn-primary">Simpan</button>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                        </form>
                    </div>
                </div>

            </div>
            <!--end row-->
            <div class="col-6 mt-4">
                <div class="col-lg-12 mt-4">
                    <div class="card border-0 rounded shadow p-4">
                        <?php echo form_open_multipart("user/bridegroom");?>
                        <div class="row mt-4">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                <label class="form-label">Calon Pengantin Wanita</label>
                                        <div>
                                        <?php
                                            if($data->bride_photo===null || $data->bride_photo===""){
                                                $img =base_url()."/assets/backend/images/icon/plus.png";
                                            }else{
                                                $img =base_url()."/uploads/bridegroom/".$data->bride_photo;
                                            }
                                            ?>
                                            <img id="imageBride" class="uploader mt-2" alt="upload" src="<?=$img?>">
                                            <input type="file" id="brideImage" style="display:none;" name="imageBride" value="">
                                        </div>
                                        <small class="text-muted">Klik + dikotak untuk mengubah</small>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="bride_name"
                                            class="form-control <?=($validation->hasError('bride_name')) ? 'is-invalid' : '';?>"
                                            placeholder="Nama Calon Pengantin Wanita" value="<?=set_value('bride_name', $data->bride_name); ?>">
                                        <label>Nama Calon Pengantin Wanita</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="bride_nickname"
                                            class="form-control <?=($validation->hasError('bride_nickname')) ? 'is-invalid' : '';?>"
                                            placeholder="Nama Panggilan Pengantin Wanita" value="<?=set_value('bride_nickname', $data->bride_nickname); ?>">
                                        <label>Nama Panggilan Pengantin Wanita</label>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="bride_about"
                                            class="form-control <?=($validation->hasError('bride_about')) ? 'is-invalid' : '';?>"
                                            placeholder="Deskripsi/Bio" value="<?=set_value('bride_about', $data->bride_about); ?>">
                                        <label>Deskripsi/Bio</label>
                                        <small class="text-muted">Contoh : Anak pertama dari bpk.samsi</small>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="form-floating mb-2">
                                        <input type="text" name="bride_ig"
                                            class="form-control <?=($validation->hasError('bride_ig')) ? 'is-invalid' : '';?>"
                                            placeholder="Username Instagram" value="<?=set_value('bride_ig', $data->bride_ig); ?>">
                                        <label>Username Instagram</label>
                                        <small class="text-muted">Contoh : @monkuid</small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-2 mb-0" align="right">
                                <button type="submit" name="submit" value="save_bride" class="btn btn-primary">Simpan</button>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                        </form>
                    </div>
                </div>
            </div>
            <!--end row-->

        </div>
    </div>
    <!--end container-->

    <?=$this->endSection();?>