<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?>  </h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/settings")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?></li>
                    </ul>
                </nav>
            </div>

            <div class="mt-4 mt-sm-0">
                <a href="<?=base_url("user/guest/create")?>" class="btn btn-primary">Tambah <span class="badge bg-danger">Sisa <?=quota("guests");?> Tamu</span></a>
            </div>
        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive bg-white shadow rounded p-4">
                                    <table class="table mb-0 table-center" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom text-center">No</th>
                                                <th class="border-bottom text-center">Kode</th>
                                                <th class="border-bottom text-center">Nama Grup</th>
                                                <th class="border-bottom text-center">Nama Tamu</th>
                                                <th class="border-bottom text-center">No Telp</th>
                                                <th class="border-bottom text-center">Email</th>
                                                <th class="border-bottom text-center py-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no=1; foreach ($guests as $d) : ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td class="text-center"><?=$d->code?></td>
                                        <td class="text-center"><?=$d->title?></td>
                                        <td class="text-center"><?=$d->full_name?></td>
                                        <td class="text-center"><?=$d->phone?></td>
                                        <td class="text-center"><?=$d->email?></td>
                                        <td class="text-center"><a href="<?=base_url('user/guest/'.encrypt($d->guest_id));?>" class="btn btn-sm btn-success">Ubah</a>
                                                    <a href="<?=base_url('user/guest/delete/'.encrypt($d->guest_id));?>" id="delete" class="btn btn-sm btn-soft-danger ms-2">Hapus</a></td>
                                    </tr>
                                    <?php $no++; endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                 
                    </div>
                </div><!--end container-->

<?=$this->endSection();?>