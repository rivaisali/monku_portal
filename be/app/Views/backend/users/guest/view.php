<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/settings")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?></li>
                    </ul>
                </nav>
            </div>
        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive bg-white shadow rounded p-4">
                                    <table class="table mb-0 table-center" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom text-center">No</th>
                                                <th class="border-bottom text-center">Kode</th>
                                                <th class="border-bottom text-center">Nama Grup</th>
                                                <th class="border-bottom text-center">Nama Tamu</th>
                                                <th class="border-bottom text-center">No Telp</th>
                                                <th class="border-bottom text-center">Email</th>
                                                <th class="border-bottom text-center py-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no=1; foreach ($guests as $d) : ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td class="text-center"><?=$d->code?></td>
                                        <td class="text-center"><?=$d->title?></td>
                                        <td class="text-center"><?=$d->full_name?></td>
                                        <td class="text-center"><?=$d->phone?></td>
                                        <td class="text-center"><?=$d->email?></td>
                                        <td class="text-center"><a href="https://wa.me/<?=substr_replace($d->no_telp, '62', 0, 1);?>?text=<?=urlencode(message($d->code,$d->nama_lengkap,$undangan,$acara));?> class="btn btn-sm btn-primary">Kirim Undangan</a>
                                                    <a href="https://<?=$d->full_domain."/".$d->code;?>" class="btn btn-sm btn-soft-primary ms-2">Lihat Undangan</a></td>
                                    </tr>
                                    <?php $no++; endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                 
                    </div>
                </div><!--end container-->


                <?php
  function message($code, $nama_lengkap,$undangan,$acara){ 
$pesan .= "*Dear $nama_lengkap*
Atas rahmat Tuhan Yang Maha Esa, mohon doa restu atas $undangan->judul kami:

*$undangan->nama_pengantin_wanita & $undangan->nama_pengantin_pria*

Kami bermaksud mengundang bapak/ibu/saudara(i) pada acara $undangan->judul kami yang akan dilaksanakan pada:\n";

foreach($acara as $t) { 
$tgl = tgl_indonesia($t->tanggal);

$pesan .= "\n*$t->nama_acara*
Hari/Tanggal: $tgl
Jam: $t->jam Wita - Selesai 
Tempat: $t->nama_lokasi
Alamat : $t->alamat_lokasi\n";

}


$pesan.="\n*Salam*
*$undangan->nama_pengantin_wanita & $undangan->nama_pengantin_pria*

========================
*Kode Undangan Anda: $d->code*
*Dapatkan petunjuk arah & konfirmasi kedatangan anda, serta berikan pesan/doa terbaik anda melalui link berikut:* 

https://$undangan->nama_domain.toduwo.id/$code

*Pesan ini dikirim melalui toduwo.in*
Made with somewhere in the world";
return $pesan;
}
?>


<?=$this->endSection();?>