<?=$this->extend('backend/main');?>
<?=$this->section('content');?>

<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/settings")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?></li>
                    </ul>
                </nav>
            </div>
<!-- 
            <div class="mt-4 mt-sm-0">
                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add-faqs">Add Questions</a>
            </div> -->
        </div>
    <div class="row">
        <div class="col-lg-12 mt-4">
            <div class="card border-0 rounded shadow p-4">
                <?php echo form_open();?>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text" name="event_name"
                                    class="form-control <?=($validation->hasError('event_name')) ? 'is-invalid' : '';?>"
                                    placeholder="Judul" value="<?=set_value('event_name', $data->event_name); ?>">
                                <label>Judul</label>
                                <small class="text-muted">Contoh: Akad Nikah, Pemberkatan, Resepsi, Unduh Mantu</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="date"
                                    class="form-control <?=($validation->hasError('date')) ? 'is-invalid' : '';?>"
                                    name="date" placeholder="Tanggal Pernikahan" value="<?=set_value('date', $data->date); ?>">
                                <label>Tanggal</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="time"
                                    class="form-control <?=($validation->hasError('time')) ? 'is-invalid' : '';?>"
                                    name="time"  placeholder="Waktu Pernikahan" value="<?=set_value('time', $data->time); ?>">
                                <label>Waktu</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text"
                                    class="form-control <?=($validation->hasError('location_name')) ? 'is-invalid' : '';?>"
                                    name="location_name"  placeholder="Nama Lokasi" value="<?=set_value('location_name', $data->location_name); ?>">
                                <label>Nama Lokasi</label>
                                <small class="text-muted">Contoh: Gedung Aziz, Rumah Mempelai</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text"
                                    class="form-control <?=($validation->hasError('latlng')) ? 'is-invalid' : '';?>"
                                    name="latlng"  placeholder="Koordinat Lokasi" value="<?=set_value('latlng', $data->latlng); ?>">
                                <label>Koordinat Lokasi</label>
                                <small class="text-muted">Contoh: 0.1212133,123.01232</small>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text"
                                    class="form-control <?=($validation->hasError('location_address')) ? 'is-invalid' : '';?>"
                                    name="location_address" placeholder="Alamat Lokasi"  value="<?=set_value('location_address', $data->location_address); ?>">
                                <label>Alamat Lokasi</label>
                                <small class="text-muted">Contoh: Jalan cinta kelurahan jodoh kecamatan
                                    pelaminan</small>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-12 mt-2 mb-0" align="right">
                        <button type="submit" name="submit" value="save" class="btn btn-primary">Simpan</button>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
                </form>
            </div>
        </div>


        <!--end col-->
    </div>
</div>
</div>
<?=$this->endSection();?>