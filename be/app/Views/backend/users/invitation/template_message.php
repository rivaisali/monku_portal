<?=$this->extend('backend/main');?>
<?=$this->section('content');?>

<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/gallery")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- 
            <div class="mt-4 mt-sm-0">
                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add-faqs">Add Questions</a>
            </div> -->
        </div>
        <div class="row">
            <div class="col-lg-8 mt-4">
                <div class="card border-0 rounded shadow p-4">
                    <?php echo form_open("user/event/create");?>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <div class="form-floating mb-2">
                                    <textarea type="text" name="title" id="title" class="form-control" 
                                     placeholder="Judul" style="height:500px"><?=$example?></textarea>
                                    <label>Judul</label>
                                    <div class="invalid-feedback d-block" id="invalid"></div>
                                    <small class="text-muted">Contoh: Akun & kamu</small>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--end row-->
                    </form>
                </div>
            </div>
        </div>
 

    </div>
</div>
<?=$this->endSection();?>