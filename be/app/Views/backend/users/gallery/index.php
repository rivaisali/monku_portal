<?=$this->extend('backend/main');?>
<?=$this->section('content');?>

<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/gallery")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- 
            <div class="mt-4 mt-sm-0">
                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add-faqs">Add Questions</a>
            </div> -->
        </div>
        <div class="row">
            <div class="col-lg-8 mt-4">
                <div class="card border-0 rounded shadow p-4">
                    <?php echo form_open("user/event/create");?>
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <div class="form-floating mb-2">
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Judul">
                                    <label>Judul</label>
                                    <div class="invalid-feedback d-block" id="invalid"></div>
                                    <small class="text-muted">Contoh: Akun & kamu</small>
                                </div>
                            </div>
                            <div class="mb-3">
                            <label class="mt-4 mb-2 badge bg-danger">Kuota Foto anda sisa <?=quota("gallery");?> Foto</label>
                                <div class="form-floating mb-2">
                                    <div id="gallery"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--end row-->
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 mt-4">
            <div class="row row-cols-md-2 row-cols-lg-4 row-cols-xl-5 row-cols-1">
               
                <?php
                foreach ($data as $d):?>
                <div class="col mt-4">
                    <div
                        class="card work-container work-modern position-relative overflow-hidden shadow rounded border-0">
                        <div class="card-body p-0">
                            <a href="<?=base_url()."/uploads/gallery/".$d->image?>" class="lightbox d-inline-block"
                                title="">
                                <img src="<?=base_url()."/uploads/gallery/".$d->image?>" class="img-fluid"
                                    alt="<?=$d->title?>">
                            </a>
                        </div>
                    </div>
                </div>
                <!--end col-->
                <?php endforeach; ?>

            </div>
            <!--end row-->
        </div>

    </div>
</div>
<?=$this->endSection();?>