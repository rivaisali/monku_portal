<?=$this->include('backend/partials/head_html');?>
    <body>
    <div class="page-wrapper landrick-theme toggled">
<?=$this->include('backend/partials/sidebar');?>
    <main class="page-content bg-light">
<?=$this->include('backend/partials/header');?>
<?=$this->renderSection('content');?>
<?=$this->include('backend/partials/footer');?>
    </main>
    </div>
<?=$this->include('backend/partials/footer_js');?>