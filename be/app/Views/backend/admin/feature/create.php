<?=$this->extend('backend/main');?>
<?=$this->section('content');?>

<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href="<?=base_url("user/settings")?>"><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?></li>
                    </ul>
                </nav>
            </div>
<!-- 
            <div class="mt-4 mt-sm-0">
                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add-faqs">Add Questions</a>
            </div> -->
        </div>
    <div class="row">
        <div class="col-lg-8 mt-4">
            <div class="card border-0 rounded shadow p-4">
                <?php echo form_open();?>
                <div class="row mt-4">
              
                    <div class="col-lg-12">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text" name="feature"
                                    class="form-control <?=($validation->hasError('feature')) ? 'is-invalid' : '';?>"
                                    placeholder="Nama Fitur">
                                <label>Nama Fitur</label>
                                   </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                            <select class="form-control" placeholder="Kategori" name="category">
                                        <option value="" readonly>Pilih</option>
                                        <option value="Basic">Basic</option>
                                        <option value="Premium">Premium</option>
                                        <option value="Custom">Custom</option>
                                    </select>
                                    <label>Pilih Kategori</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                            <select class="form-control" placeholder="Satuan" name="unit">
                                        <option value="" readonly>Pilih</option>
                                        <option value="Buah">Buah</option>
                                        <option value="Orang">Orang</option>
                                        <option value="Halaman">Halaman</option>
                                        <option value="Bulan">Bulan</option>
                                        <option value="Tahun">Tahun</option>
                                    </select>
                                    <label>Pilih Satuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text" name="price"
                                    class="form-control <?=($validation->hasError('price')) ? 'is-invalid' : '';?>"
                                    placeholder="Harga Fitur">
                                <label>Harga Fitur</label>
                                   </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="mb-3">
                            <div class="form-floating mb-2">
                                <input type="text" name="qty"
                                    class="form-control <?=($validation->hasError('qty')) ? 'is-invalid' : '';?>"
                                    placeholder="Jumlah">
                                <label>Jumlah</label>
                                   </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-2 mb-0" align="right">
                        <button type="submit" name="submit" value="save" class="btn btn-primary">Simpan</button>
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
                </form>
            </div>
        </div>


        <!--end col-->
    </div>
</div>
</div>
<?=$this->endSection();?>