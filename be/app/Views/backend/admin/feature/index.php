<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0"><?=$title?></h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a
                                href=""><?=$breadcrumb_title?></a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page"><?=$breadcrumb_subtitle?></li>
                    </ul>
                </nav>
            </div>

            <div class="mt-4 mt-sm-0">
                <a href="<?=base_url("admin/feature/create")?>" class="btn btn-primary">Tambah</a>
            </div>
        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive bg-white shadow rounded p-4">
                                    <table class="table mb-0 table-center" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom text-center">No</th>
                                                <th class="border-bottom">Nama Fitur</th>
                                                <th class="border-bottom">Jumlah</th>
                                                <th class="border-bottom">Harga</th>
                                                <th class="border-bottom">Satuan</th>
                                                <th class="border-bottom">Kategori</th>
                                                <th class="border-bottom text-center py-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no=1; foreach ($features as $d) : ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td><?=$d->feature?></td>
                                        <td><?=$d->qty?></td>
                                        <td><?=rupiah($d->price)?></td>
                                        <td><?=$d->unit?></td>
                                        <td><?=$d->category?></td>
                                        <td class="text-center"><a href="<?=base_url('admin/feature/'.encrypt($d->id));?>" class="btn btn-sm btn-success">Ubah</a>
                                                    <a href="<?=base_url('admin/feature/delete/'.encrypt($d->id));?>" class="btn btn-sm btn-soft-danger ms-2 delete">Hapus</a></td>
                                    </tr>
                                    <?php $no++; endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                 
                    </div>
                </div><!--end container-->

<?=$this->endSection();?>