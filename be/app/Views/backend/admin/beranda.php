<?=$this->extend('backend/main');?>
<?=$this->section('content');?>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-flex align-items-center justify-content-between">
            <div>
                <h6 class="text-muted mb-1">Selamat datang, <?=user("full_name");?>!</h6>
                <h5 class="mb-0">Dashboard</h5>
            </div>
        </div>

        <div class="row row-cols-xl-2 row-cols-md-2 row-cols-1">
            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <i class="uil uil-users-alt fs-4 mb-0"></i>
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Pengguna</h6>
                            <p class="fs-5 text-dark fw-bold mb-0"><?=$count_users->count?></p>
                        </div>
                    </div>
                </a>
            </div>
            <!--end col-->

            <div class="col mt-4">
                <a href="#!"
                    class="features feature-primary d-flex justify-content-between align-items-center bg-white rounded shadow p-3">
                    <div class="d-flex align-items-center">
                        <div class="icon text-center rounded-pill">
                            <i class="uil uil-user-circle fs-4 mb-0"></i>
                        </div>
                        <div class="flex-1 ms-3">
                            <h6 class="mb-0 text-muted">Total Pemasukan</h6>
                            <p class="fs-5 text-dark fw-bold mb-0"></p>
                        </div>
                    </div>
                </a>
            </div>
            <!--end col-->
            <!--end col-->
        </div>

        <div class="row">
        <div class="col-xl-12 col-lg-12 mt-4 rounded">
                <div class="card shadow border-0">
                    <div class="p-4 border-bottom">
                        <div class="d-flex justify-content-between">
                            <h6 class="mb-0 fw-bold">Orderan Baru</h6>                           
                        </div>
                    </div>
                    <div class="table-responsive bg-white shadow rounded p-4">
                                    <table class="table mb-0 table-center" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom text-center">No</th>
                                                <th class="border-bottom text-center">Nama</th>
                                                <th class="border-bottom text-center">Paket</th>
                                                <th class="border-bottom text-center">Harga</th>
                                                <th class="border-bottom text-center">Status</th>
                                                <th class="border-bottom text-center py-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no=1; foreach ($data as $d) : ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td class="text-center"><?=$d->full_name?></td>
                                        <td class="text-center"><?=$d->package?></td>
                                        <td class="text-center"><?=rupiah($d->total)?></td>
                                        <td class="text-center">
                                        <?php
                                        if($d->transaction_status=="0") echo"<span class='badge bg-danger'>Belum Lunas</span>";
                                        else if($d->transaction_status=="1") echo"<span class='badge bg-warning'>Pending</span>"; 
                                         elseif($d->transaction_status=="2") echo"<span class='badge bg-success'>Lunas</span>";      
                                        ?>
                                    
                                    </td>
                                        <td class="text-center">
                                            <!-- <a href="<?=base_url('user/guest/'.encrypt($d->transactions_id));?>" class="btn btn-sm btn-success">Ubah</a> -->
                                            <!-- <a href="<?=base_url('user/guest/delete/'.encrypt($d->transactions_id));?>" class="btn btn-sm btn-soft-danger ms-2">Hapus</a> -->
                                        </td>
                                    </tr>
                                    <?php $no++; endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                    
                    <!--end col-->
                    <!--end row-->
                </div>
                <!--end col-->
              
                
            </div>
                        </div><!--end row-->
                 
        <!--end row-->
    </div>
    <!--end container-->
    <?=$this->endSection();?>