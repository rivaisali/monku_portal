
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Daftar - Monku Indonesia</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Solusi pernikahan lebih hemat, praktis, dan kekinian dengan e-invitation yang disebar otomatis untuk memberikan kesan terbaik. Daftar sekarang, Gratis!">
        <meta name="keywords" content="Undangan, Digital, Monku, Pernikahan, Wedding, Undangan Digital" />
        <meta name="author" content="Arajang Digital Indonesia" />
        <!-- favicon -->
        <link rel="shortcut icon" href="<?=base_url("assets/favicon.png")?>">
        <link href="<?=base_url("assets/backend/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="<?=base_url("assets/backend/css/simplebar.css")?>" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="<?=base_url("assets/backend/css/materialdesignicons.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("assets/backend/css/tabler-icons.min.css")?>" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">

        <!-- Css -->
        <link href="<?=base_url("assets/backend/css/style.css")?>" rel="stylesheet" type="text/css" id="theme-opt" />
    </head>

    <body>
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->

        <!-- Hero Start -->
        <section class="bg-home bg-circle-gradiant d-flex align-items-center">
            <div class="bg-overlay bg-overlay-white"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="form-signin p-4 bg-white rounded shadow">
                        <?php echo form_open(base_url('daftar')); ?>
                            <a href="index.html"><img src="<?=base_url()?>/assets/backend/images/logo_icon.png" class="avatar avatar-small mb-4 d-block mx-auto" alt=""></a>
                                <h5 class="mb-3 text-center">Daftar</h5>
                             
                                <div class="form-floating mb-2">
                                    <input type="text" class="form-control <?=($validation->hasError('full_name')) ? 'is-invalid' : '';?>" name="full_name" value="<?=old('full_name')?>" placeholder="Nama Lengkap">
                                    <label for="floatingInput">Nama Lengkap</label>
                                    <div class="invalid-feedback d-block"><?=$validation->getError("full_name");?></div>
                                </div>

                                <div class="form-floating mb-2">
                                    <input type="email" class="form-control <?=($validation->hasError('email')) ? 'is-invalid' : '';?>" name="email" value="<?=old('email')?>" placeholder="name@example.com">
                                    <label for="floatingEmail">Email</label>
                                    <div class="invalid-feedback d-block"><?=$validation->getError("email");?></div>
                                </div>

                                <div class="form-floating mb-2">
                                    <input type="text" maxlength="13" class="form-control <?=($validation->hasError('phone')) ? 'is-invalid' : '';?>"  value="<?=old('phone')?>" name="phone" placeholder="08****">
                                    <label for="floatingPhone">No Telp</label>
                                    <div class="invalid-feedback d-block"><?=$validation->getError("phone");?></div>
                                </div>

                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control <?=($validation->hasError('password')) ? 'is-invalid' : '';?>" name="password" placeholder="Password">
                                    <label for="floatingPassword">Password</label>
                                    <div class="invalid-feedback d-block"><?=$validation->getError("password");?></div>
                                </div>
                            
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">Dengan mendaftar, saya menyetujui  <a href="#" class="text-primary">Syarat dan Ketentuan</a>
                                    dan<a href="#" class="text-primary"> Kebijakan Privasi.</a></label>
                                </div>
                
                                <button class="btn btn-primary w-100" name="submit" value="submit" type="submit">Miliki Sekarang</button>

                                <div class="col-12 text-center mt-3">
                                 
                                    <p class="mb-0 mt-3"><small class="text-dark me-2">Sudah punya akun ?</small> <a href="/login" class="text-dark fw-bold">Login</a></p>
                                </div><!--end col-->

                                <p class="mb-0 text-muted mt-3 text-center">© <script>document.write(new Date().getFullYear())</script> Monku.id.</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->
        
      <!-- javascript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="<?=base_url("assets/backend/js/bootstrap.bundle.min.js")?>"></script>
        <!-- simplebar -->
        <script src="<?=base_url("assets/backend/js/simplebar.min.js")?>"></script>
        <!-- Icons -->
        <script src="<?=base_url("assets/backend/js/feather.min.js")?>"></script>
        <!-- Main Js -->
        <script src="<?=base_url("assets/backend/js/plugins.init.js")?>"></script>
        <script src="<?=base_url("assets/backend/js/app.js")?>"></script>
        <script src="<?=base_url("assets/backend/js/custom.js")?>"></script>
        
    </body>

</html>