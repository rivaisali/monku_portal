<nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content" data-simplebar style="height: calc(100% - 60px);">
        <div class="sidebar-brand text-center">
            <a href="index.html">
                <img src="<?=base_url()?>/assets/backend/images/logo.png" height="48" class="logo-light-mode" alt="">
                <img src="<?=base_url()?>/assets/backend/images/logo.png" height="48" class="logo-dark-mode" alt="">
                <span class="sidebar-colored">
                    <img src="<?=base_url()?>/assets/backend/images/logo.png" height="48" alt="">
                </span>
            </a>
        </div>

        <?php
        if(user("role") === "user"){ ?>
        <ul class="sidebar-menu">
            <li><a href="<?=base_url(user("role"))?>"><i class="ti ti-home me-2"></i>Dashboard</a></li>
            <li>
                <a href="<?=base_url("user/settings")?>"><i class="ti ti-settings me-2"></i>Pengaturan</a>
            </li>
            <li>
                <a href="<?=base_url("user/group")?>"><i class="ti ti-link me-2"></i>Grup Tamu</a>
            </li>
            <li>
                <a href="<?=base_url("user/guest")?>"><i class="ti ti-user me-2"></i>Tamu</a>
            </li>
            <li>
                <a href="<?=base_url("user/send")?>"><i class="ti ti-mail-opened me-2"></i>Kirim Undangan</a>
            </li>
            <li>
                <a href="<?=base_url("user/settings")?>"><i class="ti ti-book me-2"></i>Penerima Tamu</a>
            </li>
        </ul>
        <?php }else if(user("role") === "admin"){?>
        <ul class="sidebar-menu">
            <li><a href="<?=base_url(user("role"))?>"><i class="ti ti-home me-2"></i>Dashboard</a></li>
            <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="ti ti-file-invoice me-2"></i>Master</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="<?=base_url("admin/package")?>">Paket</a></li>
                                    <li><a href="<?=base_url("admin/feature")?>">Fitur</a></li>
                                </ul>
                            </div>
                        </li>
          
        </ul>
        <?php } ?>
        <!-- sidebar-menu  -->
    </div>
    <!-- Sidebar Footer -->
    <ul class="sidebar-footer list-unstyled mb-0">
        <li class="list-inline-item mb-0">
            <a href="https://1.envato.market/4n73n" target="_blank" class="btn btn-icon btn-soft-light"><i
                    class="ti ti-shopping-cart"></i></a> <small class="text-muted ms-1">Buy Now</small>
        </li>
    </ul>
    <!-- Sidebar Footer -->
</nav>
<!-- sidebar-wrapper  -->