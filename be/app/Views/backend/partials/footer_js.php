 <!-- javascript -->
 <script src="<?=base_url("assets/backend/js/bootstrap.bundle.min.js");?>"></script>
        <!-- simplebar -->
        <script src="<?=base_url("assets/backend/js/simplebar.min.js");?>"></script>
        <!-- Icons -->
        <script src="<?=base_url("assets/backend/js/feather.min.js");?>"></script>
        <!-- Chart -->
        <script src="<?=base_url("assets/backend/js/apexcharts.min.js");?>"></script>
         <!-- Lightbox -->
         <script src="<?=base_url("assets/backend/js/tobii.min.js");?>"></script>
        <!-- Main Js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.5/bootstrap-notify.min.js"></script>
        <script src="https://transloadit.edgly.net/releases/uppy/v1.6.0/uppy.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <script src="<?=base_url("assets/backend/js/plugins.init.js");?>"></script>
        <script src="<?=base_url("assets/backend/js/app.js");?>"></script>
        <script src="<?=base_url("assets/backend/js/custom.js")?>"></script>
        <script>

      $(document).ready(function () {
        $('#dataTable').DataTable();
      });

   
      $('.delete').confirm({
          title: "Konfirmasi",
          theme: 'material',
          content: "Anda ingin menghapus data ini ?",
          icon: 'ti ti-help',
          theme: 'material',
          animation: 'scale',
          type: 'red',
          buttons: {
            Hapus: {
              btnClass: 'btn-primary',
              action: function () {
                location.href = this.$target.attr('href');
              }

            },
            Batal: {
              btnClass: 'btn-default',
            }
          }
        });

    var uppy = Uppy.Core({
        debug: false,
        autoProceed: false,
        restrictions: {
          maxFileSize: 1024000,
          maxNumberOfFiles: max_image,
          minNumberOfFiles: 1,
          allowedFileTypes: ['image/*']
        }
      })

      .use(Uppy.Dashboard, {
        inline: true,
        target: '#gallery',
        width: 750,
        height: 250
      })

    uppy.on('file-added', (file) => {

        if ($("#title").val() === "" || $("#title").val().length < 3) {
          $('#title').addClass("is-invalid");
          $('#invalid').html("Judul harus diisi");
          $('#title').focus();
          uppy.cancelAll();
        } else {
          $('#title').removeClass("is-invalid");
          $('#title').addClass("is-valid");
          $('#invalid').html("");
          uppy.setFileMeta(file.id, {
            title: $("#title").val()
          });
        }

      })


      .use(Uppy.XHRUpload, {
        endpoint: url + '/user/gallery/create',
        formData: true,
        fieldName: 'images',
        metaFields: ['title'],

      })
    uppy.run();
    uppy.on('complete', (result) => {
      location.reload();
    })
    </script>
    </body>

</html>