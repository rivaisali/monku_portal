<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <?php
        if(!isset($title)){
          echo"<title>Undangan Digital - Monku Indonesia</title>"; 
        }else{
         echo"<title>$title - Undangan Digital Monku Indonesia</title>"; 
        }
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Solusi pernikahan lebih hemat, praktis, dan kekinian dengan e-invitation yang disebar otomatis untuk memberikan kesan terbaik. Daftar sekarang, Gratis!">
        <meta name="keywords" content="Undangan, Digital, Monku, Pernikahan, Wedding, Undangan Digital" />
        <meta name="author" content="Arajang Digital Indonesia" />
        <!-- favicon -->
        <link rel="shortcut icon" href="<?=base_url("assets/favicon.png")?>">
        <!-- Bootstrap -->
        <link href="<?=base_url("assets/backend/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="<?=base_url("assets/backend/css/simplebar.css");?>" rel="stylesheet" type="text/css" />
        
        <!-- Icons -->
        <link href="<?=base_url("assets/backend/css/materialdesignicons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("assets/backend/css/tabler-icons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"   rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css"  rel="stylesheet">
        <link href="https://transloadit.edgly.net/releases/uppy/v1.6.0/uppy.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

        <!-- Lightbox -->
        <link href="<?=base_url("assets/backend/css/tobii.min.css");?>" rel="stylesheet" type="text/css" />

      <!-- Css -->
        <link href="<?=base_url("assets/backend/css/style.css");?>" rel="stylesheet" type="text/css" id="theme-opt" />
        <script>
        const url = '<?=base_url();?>';
        <?php if(isset($max_image)){?>
        const max_image = <?=$max_image?>;
        <?php } ?>
    </script>
    </head>
   