<div class="top-header">
<?php if (session()->getFlashdata("notification")) {?>
                    <div class="flash-data" data-statusflashdata="<?=session()->getFlashdata("notification")["status"];?>" data-msgflashdata="<?=session()->getFlashdata("notification")["msg"];?>"></div>
                <?php }?>
                    <div class="header-bar d-flex justify-content-between border-bottom">
                        <div class="d-flex align-items-center">
                            <a href="#" class="logo-icon me-3">
                              
                            <img src="<?=base_url()?>/assets/backend/images/logo.png" height="30" class="small" alt="">
                                <span class="big">
                                    <img src="<?=base_url()?>/assets/backend/images/logo.png" height="64" class="logo-light-mode" alt="">
                                    <img src="<?=base_url()?>/assets/backend/images/logo.png" height="64" class="logo-dark-mode" alt="">
                                </span>
                               
                            </a>
                            <a id="close-sidebar" class="btn btn-icon btn-soft-light" href="javascript:void(0)">
                                <i class="ti ti-menu-2"></i>
                            </a>
              
                                        
                                     
                            <div class="p-0 d-none d-md-block ms-2">
                            </div>
                        </div>
        
                        <ul class="list-unstyled mb-0">
                            <li class="list-inline-item mb-0">
                                <a href="<?=base_url()?>/start">
                               
                                    <div class="btn btn-icon btn-soft-light"><i class="ti ti-settings"></i></div>
                                </a>
                            </li>

                  
                            <li class="list-inline-item mb-0 ms-1">
                                <div class="dropdown dropdown-primary">
                                    <button type="button" class="btn btn-soft-light dropdown-toggle p-0" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?=base_url()?>/assets/backend/images/client/user_placeholder.png" class="avatar avatar-ex-small rounded" alt=""></button>
                                    <div class="dropdown-menu dd-menu dropdown-menu-end bg-white shadow border-0 mt-3 py-3" style="min-width: 200px;">
                                        <a class="dropdown-item d-flex align-items-center text-dark pb-3" href="javascript:;">
                                            <img src="<?=base_url()?>/assets/backend/images/client/user_placeholder.png" class="avatar avatar-md-sm rounded-circle border shadow" alt="">
                                            <div class="flex-1 ms-2">
                                                <span class="d-block"><?=user("full_name");?></span>
                                                <small class="text-muted"><?=user("email");?></small>
                                            </div>
                                        </a>
                                        <?php
                                $request = \Config\Services::request();
                                 $uri =  $request->uri->getSegment(1)."/".$request->uri->getSegment(2);
                                 if($uri==="user/start" || $uri==="user/payment" || $uri==="user/step"){ ?>
 <div class="dropdown-divider border-top"></div>
                                      <a class="dropdown-item text-dark" href="<?=base_url()?>/auth/logout"><span class="mb-0 d-inline-block me-1"><i class="ti ti-logout"></i></span> Logout</a>
                                 
                                 <?php }else{?>

                                        <a class="dropdown-item text-dark" href="<?=base_url()?>/user"><span class="mb-0 d-inline-block me-1"><i class="ti ti-home"></i></span> Dashboard</a>
                                        <a class="dropdown-item text-dark" href="<?=base_url()?>/user/settings"><span class="mb-0 d-inline-block me-1"><i class="ti ti-settings"></i></span> Pengaturan</a>
                                        <div class="dropdown-divider border-top"></div>
                                        <a class="dropdown-item text-dark" href="<?=base_url()?>/user/start"><span class="mb-0 d-inline-block me-1"><i class="ti ti-lock"></i></span> Ganti Undangan</a>
                                        <a class="dropdown-item text-dark" href="<?=base_url()?>/auth/logout"><span class="mb-0 d-inline-block me-1"><i class="ti ti-logout"></i></span> Logout</a>
                                 
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>