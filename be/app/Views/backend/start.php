<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Mulai - Monku Indonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="Solusi pernikahan lebih hemat, praktis, dan kekinian dengan e-invitation yang disebar otomatis untuk memberikan kesan terbaik. Daftar sekarang, Gratis!">
    <meta name="keywords" content="Undangan, Digital, Monku, Pernikahan, Wedding, Undangan Digital" />
    <meta name="author" content="Arajang Digital Indonesia" />
    <!-- favicon -->
    <link rel="shortcut icon" href="<?=base_url("assets/favicon.png")?>">
    <!-- Bootstrap -->
    <link href="<?=base_url("assets/backend/css/bootstrap.min.css")?>" rel="stylesheet" type="text/css" />
    <!-- simplebar -->
    <link href="<?=base_url("assets/backend/css/simplebar.css")?>" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="<?=base_url("assets/backend/css/materialdesignicons.min.css")?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url("assets/backend/css/tabler-icons.min.css")?>" rel="stylesheet" type="text/css" />
    <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

    <!-- Css -->
    <link href="<?=base_url("assets/backend/css/style.css")?>" rel="stylesheet" type="text/css" id="theme-opt" />
    <script>
        const url = '<?=base_url();?>';
    </script>
</head>

<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <!-- Loader -->

    <!-- Hero Start -->
    <section class="bg-home bg-circle-gradiant d-flex align-items-center">
    <?php if (session()->getFlashdata("notification")) {?>
                    <div class="flash-data" data-statusflashdata="<?=session()->getFlashdata("notification")["status"];?>" data-msgflashdata="<?=session()->getFlashdata("notification")["msg"];?>"></div>
                <?php }?>
        <div class="bg-overlay bg-overlay-white"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-8">
                    <!-- <img src="<?=base_url()?>/assets/backend/images/logo.png" height="64" class="mx-auto d-block" alt=""> -->
                    <div class="card login-page bg-white shadow mt-4 rounded border-0">
                        <div class="card-body">
                            <h4 class="text-center">Pilih Undangan Anda</h4>
                            <h6 class="text-center" style="font-size:12px;font-weight:normal;">Silahkan pilih undangan yang akan dikelola dibawah ini</h6>
                            <?php echo form_open(); ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3 text-center">
                                            <img src="<?=base_url()?>/assets/backend/images/logo_icon.png"
                                                class="avatar avatar-md-md rounded-pill mx-auto d-block shadow" alt="">

                                            <div class="mt-3">
                                                <h5 class="mb-1"><?=user("full_name")?></h5>
                                                <p class="text-muted mb-0"><?=user("email");?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    foreach($data as $d):?>
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <div class="d-grid">
                                                <button class="btn btn-outline-primary btn-block" name="choose" value="<?=encrypt($d->id)?>">
                                                    <?=$d->title?>
                                            <small style="font-size:12px;font-weight:normal;">(<?=$d->full_domain?>)</small>
                                            </button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </form>
                            <div class="col-lg-12 mb-0">
                                            <div class="d-grid">
                                                <a class="btn btn-primary" href="<?=base_url()?>/user/invitation/create">Buat Undangan Baru</a>
                                            </div>
                                        </div>
                        </div>
                    </div>
                    <!---->
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section-->
    <!-- Hero End -->
    <!-- javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?=base_url("assets/backend/js/bootstrap.bundle.min.js")?>"></script>
    <!-- simplebar -->
    <script src="<?=base_url("assets/backend/js/simplebar.min.js")?>"></script>
    <!-- Icons -->
    <script src="<?=base_url("assets/backend/js/feather.min.js")?>"></script>
    <!-- Main Js -->
    <script src="<?=base_url("assets/backend/js/plugins.init.js")?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.5/bootstrap-notify.min.js">
    </script>
    <script src="<?=base_url("assets/backend/js/app.js")?>"></script>
    <script src="<?=base_url("assets/backend/js/custom.js")?>"></script>

    </script>
</body>

</html>