<?=$this->extend('frontend/main');?>
<?=$this->section('content');?>

        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!-- Hero End -->

        
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 filters-group-wrap">
                        <div class="filters-group">
                            <ul class="container-filter list-inline mb-0 filter-options text-center">
                                <li class="list-inline-item categories-name border text-dark rounded active" data-group="all">All</li>
                                <!-- <li class="list-inline-item categories-name border text-dark rounded" data-group="business">Business</li>
                                <li class="list-inline-item categories-name border text-dark rounded" data-group="marketing">Marketing</li>
                                <li class="list-inline-item categories-name border text-dark rounded" data-group="finance">Finance</li>
                                <li class="list-inline-item categories-name border text-dark rounded" data-group="human">Human Research</li>
                            </ul> -->
                        </div>
                    </div><!--end col-->
                </div><!--end row-->

                <div id="grid" class="row">
<?php foreach($theme as $d) : ?>
                    <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2 picture-item" data-groups='["all"]'>
                        <div class="card blog border-0 work-container work-classic shadow rounded-md overflow-hidden">
                            <img src="<?=base_url()?>/uploads/themes/<?=$d->image?>" class="img-fluid work-image" alt="">
                            <div class="card-body">
                                <div class="content">
                                    <a href="javascript:void(0)" class="badge badge-link bg-primary">Tema</a>
                                    <h5 class="mt-3"><a href="page-case-detail.html" class="text-dark title"><?=$d->title?></a></h5>
                                    <p class="text-muted"><?=$d->description?></p>
                                    <a href="<?=$d->demo_link?>" target="_blank" class="text-primary h6">Lihat Demo <i class="uil uil-angle-right-b align-middle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
<?php endforeach; ?>
        
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <?=$this->endSection();?>