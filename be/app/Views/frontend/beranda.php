<?=$this->extend('frontend/main');?>
<?=$this->section('content');?>
<!-- Hero Start -->
      <section class="bg-half-170 bg-light d-table w-100" style="background: url('<?=base_url()?>/assets/frontend/images/bg.png') center center; height: auto;">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-5 col-12">
                        <div class="title-heading">
                           
                            <h2 class="fw-bold mt-2 mb-3">Platform <br>
                                Undangan Digital <span class="text-primary typewrite" data-period="2000" data-type='[ "Terbaik", "Murah", "Cepat"]'> </span></h2>
                            
                            <p class="para-desc text-muted">Solusi pernikahan lebih hemat, praktis, dan kekinian dengan e-invitation yang disebar otomatis.</p>
                            
                            <div class="mt-4 pt-2">
                                <a href="<?=base_url()?>/login" class="btn btn-primary me-2">Buat Sekarang</a>
                                <a href="javascript:void(0)" class="btn btn-outline-primary">Lihat Tema</a>
                            </div>
                        </div>
                    </div><!--end col-->

                    <div class="col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <img src="<?=base_url()?>/assets/frontend/images/section_home.png" width="900" class="img-fluid d-block mx-auto" alt="">
                    </div><!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->

        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!-- Hero End -->

        <!-- Start fitur -->
        <section class="section" id="home">
            <div class="container mt-100 mt-60">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="me-lg-5">
                            <img src="<?=base_url()?>/assets/frontend/images/section_about.png" class="img-fluid" alt="">
                        </div>
                    </div><!--end col-->

                    <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="section-title">
                            <h4 class="title mb-4">Undangan Digital Yang Berkesan</h4>
                            <p class="text-muted">90% undangan fisik hanya akan dibuang begitu saja. Kami memberikan website sebagai undangan digital yang dapat dikirim melalui WhatsApp atau email secara otomatis!</p>
                            <ul class="list-unstyled text-muted">
                                <li class="mb-1"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>Lebih Dekat dengan Para Tamu</li>
                                <li class="mb-1"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>Check-in di Lokasi</li>
                                <li class="mb-1"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span>Cara Baru Memberi Donasi</li>
                            </ul>
                 
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="container mt-100 mt-60">
                <div class="row align-items-center">
                    <div class="col-md-6 order-1 order-md-2">
                        <div class="ms-lg-5">
                            <img src="<?=base_url()?>/assets/frontend/images/section_contact2.png"  class="img-fluid" alt="">
                        </div>
                    </div><!--end col-->

                    <div class="col-md-6 order-2 order-md-1 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="section-title">
                            <h4 class="title mb-4">Layanan bantuan yang selalu <br>aktif dan cepat</h4>
                            <p class="text-muted">Monku.id memberikan pelayanan yang maksimal kepada client dengan Pelayanan 24/7 jam 
                                agar semua permasalahan client dapat diselesaikan dengan cepat.</p>
                                <p class="text-muted">  yuk Tunggu apa lagi, gabung dengan beberapa pasangan lainnya yang sudah menggunakan undangan website kami, bikin cuma sekali, kirimnya berkali kali</p>
                           
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End -->
               <!-- Description Start -->
               <section class="section" id="features">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="section-title mb-4 pb-2">
                                <h4 class="title mb-4">Fitur Monku</h4>
                                <p class="text-muted para-desc mx-auto mb-0">Solusi Undangan yang lebih hemat, praktis, dan kekinian dengan e-invitation yang disebar otomatis</p>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
    
                    <div class="row">
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
      
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="tablet" class="fea icon-ex-md text-primary h1"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Website Selalu Aktif</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Website yang cepat dan aman akan aktif tanpa ada batasan waktu.</p>
                           
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="globe" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Custom Domain</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Tampil unik dengan domain kamu sendiri (namakamu.com)</p>
                           
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="image" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Cerita</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Tuangkan cerita, galeri foto, hingga video ke dalam website.</p>
                           
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="book" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Buku Tamu Digital</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Catatan kehadiran dapat dipantau dengan mudah dan lengkap.</p>
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="message-square" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Doa dan Ucapan</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Para tamu dapat memberikan doa dan ucapan langsung di website-mu.</p>
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="heart" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Donasi Digital</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Para tamu dapat memberikan doa dan ucapan langsung di website-mu.</p>
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="map" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Navigasi Peta</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Memudahkan tamu menemukan lokasi acara kamu dengan bantuan navigasi peta.</p>
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="calendar" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Sync Dengan Google Calendar</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Tamu tidak akan lupa dengan acara kamu yang akan berlangsung</p>
                                </div>
                            </div>
                        </div><!--end col-->
                        
                        <div class="col-lg-4 col-md-6 mt-4 pt-2">
                            <div class="d-flex key-feature align-items-center p-3">
                                <div class="icon text-center rounded-circle me-3">
                                    <i data-feather="credit-card" class="fea icon-ex-md text-primary"></i>
                                </div>
                                <div class="flex-1">
                                    <h4 class="title mb-0">Pembayaran Mudah</h4>
                                    <p class="text-muted mt-2" style="font-size:0.8em">Semua transaksi dapat dilakukan secara mudah, online, dan instan.</p>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div><!--end container-->
            </section><!--end section-->
            <!-- Description End -->
<section class="section" id="price">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <h4 class="title mb-4">Harga Bersahabat Sesuai Dengan Kebutuhanmu</h4>
                    <p class="text-muted para-desc mb-0 mx-auto">Semua paket sudah mendapatkan halaman website, manajemen tamu, RSVP, komentar, dan buku tamu digital!</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row align-items-end">
            <?php foreach ($packages as $d): ?>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <!-- <div class="pricing text-center rounded overflow-hidden shadow"> -->
                <div class="pricing text-center rounded overflow-hidden <?=$d->favorite==false ? "shadow":"shadow-lg"?>">
                                   
                    <?php if($d->favorite==false){ ?>
                    <div class="price-header border-bottom pt-5 pb-5">
                        <h1 class="text-primary"><i class="uil <?=$d->icon?>"></i></h1>
                        <h5 class="price-title"><?=$d->package?></h5>
                        <p class="mb-0 text-muted">Reguler</p>
                    </div>
                    <div class="border-bottom py-4">
                         <h4><del><?=rupiah($d->price)?></del><sup><?=$d->discount?>%</sup></h4>
                        <h2 class="fw-bold"><?=rupiah($d->price-($d->price * $d->discount / 100))?> </h2>
                        <!-- <h6 class="text-muted mb-0 fw-normal">Billed monthly per user</h6> -->
                        <a href="<?=base_url()?>/login" class="btn btn-primary mt-4">Buat Sekarang</a>                                     
                    </div>
                    <?php } else {?>
                        <div class="price-header border-bottom bg-primary pt-5 pb-5">
                        <h1 class="text-white-50"><i class="uil uil-award"></i></h1>
                        <h5 class="price-title text-white"><?=$d->package?></h5>
                        <p class="mb-0 text-light">Populer</p>
                    </div>
                    <div class="border-bottom py-5">
                    <h4><del><?=rupiah($d->price)?></del><sup><?=$d->discount?>%</sup></h4>
                        <h2 class="fw-bold"><?=rupiah($d->price-($d->price * $d->discount / 100))?> </h2>
                        <!-- <h6 class="text-muted mb-0 fw-normal">Billed monthly per user</h6> -->
                        <a href="<?=base_url()?>/daftar" class="btn btn-primary mt-4">Buat Sekarang</a>                                     
                    </div>
                    <?php }?>
                    
                    <div class="pricing-features text-start p-4">
                        <h5>Fitur :</h5>
                        <ul class="feature list-unstyled mb-0">
                            <?php
                            $model = new \App\Models\CustomModel();
                            $data = $model->select_data_join("*,feature_package.id as feature_package_id", "feature_package", "getResult", [
                                [
                                    "table" => "packages",
                                    "cond" => "packages.id = feature_package.package_id",
                                    "type" => "",
                                ],
                                [
                                    "table" => "features",
                                    "cond" => "features.id = feature_package.feature_id",
                                    "type" => "",
                                ],
                               
                            ],["feature_package.package_id" => $d->id]);
                            foreach ($data as $dt): 
                            ?>
                            <li class="h6 text-muted mb-0"><span class="text-primary h5 me-2"><i class="uil uil-check-circle align-middle"></i></span><?=$dt->quota." ".$dt->feature?></li>
                           <?php endforeach; ?>
                        </ul>
                    </div>    
                </div><!--end price three-->
            </div><!--end col-->
            
            <?php endforeach; ?>

            
      
        </div><!--end row-->
    </div><!--end container-->
</section>
<?=$this->endSection();?>