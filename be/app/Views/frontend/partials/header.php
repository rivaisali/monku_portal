<body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <!-- Navbar STart -->
        <header id="topnav" class="defaultscroll sticky">
            <div class="container">
                <!-- Logo container-->
                <a class="logo" href="<?=base_url()?>">
                    <img src="<?=base_url("assets/frontend/images/logo.png");?>" height="50" class="logo-light-mode" alt="">
                    <img src="<?=base_url("assets/frontend/images/logo.png");?>" height="50" class="logo-dark-mode" alt="">
                </a>
                
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle" id="isToggle" onclick="toggleMenu()">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                </div>

                <ul class="buy-button list-inline mb-0">
                  
                    <li class="list-inline-item sidebar-icon mb-0">
                        <a href="https://facebook.com/monku_id" target="_blank">
                            <div class="btn btn-icon btn-soft-primary"><i data-feather="facebook" class="fea icon-sm"></i></div>
                        </a>
                    </li>

                    <li class="list-inline-item button mb-0 ps-1">
                        <a href="https://instagram.com/monku_id" target="_blank" class="btn btn-icon btn-soft-primary"><i class="uil uil-instagram"></i></a>
                    </li>
                    <li class="list-inline-item button mb-0 ps-1">
                        <a href="<?=base_url("login")?>" class="btn btn-icon btn-soft-primary"><i class="uil uil-user"></i></a>
                    </li>
                </ul><!--end login button-->
        
                <div id="navigation">
                    <!-- Navigation Menu-->   
                    <ul class="navigation-menu">
                        <li><a href="<?=base_url()?>/#home" class="sub-menu-item">Home</a></li>
                        <li><a href="<?=base_url()?>/#features" class="sub-menu-item">Fitur</a></li>
                        <li><a href="<?=base_url()?>/tema" class="sub-menu-item">Tema</a></li>
                        <li><a href="<?=base_url()?>/#price" class="sub-menu-item">Harga</a></li>
                    </ul><!--end navigation menu-->
                </div><!--end navigation-->
            </div><!--end container-->
        </header><!--end header-->
        <!-- Navbar End -->