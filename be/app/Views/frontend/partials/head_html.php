<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Solusi pernikahan lebih hemat, praktis, dan kekinian dengan e-invitation yang disebar otomatis untuk memberikan kesan terbaik. Daftar sekarang, Gratis!">
        <meta name="keywords" content="Undangan, Digital, Monku, Pernikahan, Wedding, Undangan Digital" />
        <meta name="author" content="Arajang Digital Indonesia" />
    <link rel="icon" href="<?=base_url("assets/favicon.png");?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url("assets/favicon.png");?>" type="image/x-icon">
    <title>Monku | Undangan Digital - Platform Undangan Digital Terbaik</title>
        <!-- Bootstrap -->
        <link href="<?=base_url("assets/frontend/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="<?=base_url("assets/frontend/css/materialdesignicons.min.css");?>" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.6/css/line.css">
        <!-- Slider -->               
        <link rel="stylesheet" href="<?=base_url("assets/frontend/css/tiny-slider.css");?>"/> 
        <!-- Main Css -->
        <link href="<?=base_url("assets/frontend/css/style.css");?>" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="<?=base_url("assets/frontend/css/colors/default.css");?>" rel="stylesheet" id="color-opt">
     <script>
        const url = '<?=base_url();?>';
    </script>
  </head>