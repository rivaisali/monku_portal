  <!-- Back to top -->
  <a href="#" onclick="topFunction()" id="back-to-top" class="back-to-top fs-5" style="margin-bottom:50px";><i data-feather="arrow-up" class="fea icon-sm icons align-middle"></i></a>
        <!-- Back to top -->

        <!-- javascript -->
        <script src="<?=base_url("assets/frontend/js/bootstrap.bundle.min.js");?>"></script>
        <!-- SLIDER -->
        <script src="<?=base_url("assets/frontend/js/tiny-slider.js");?>"></script>
        <!-- Icons -->
        <script src="<?=base_url("assets/frontend/js/feather.min.js");?>"></script>
        <!-- Main Js -->
        <script src="<?=base_url("assets/frontend/js/plugins.init.js");?>"></script><!--Note: All init js like tiny slider, counter, countdown, maintenance, lightbox, gallery, swiper slider, aos animation etc.-->
        <script src="<?=base_url("assets/frontend/js/app.js");?>"></script><!--Note: All important javascript like page loader, menu, sticky menu, menu-toggler, one page menu etc. -->
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>

      </body>
</html>