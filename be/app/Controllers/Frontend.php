<?php

namespace App\Controllers;

class Frontend extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
        $data['packages'] = $this->model->select_data("packages", "getResult");
        // $data['packages'] = $this->model->select_data_join("*", "packages", "getResult", [
        //         [
        //             "table" => "packages",
        //             "cond" => "packages.id = guests.group_id",
        //             "type" => "",
        //         ],
               
        //     ],
        //     ["guests.user_id" => user("user_id"), "guests.invitation_id" => invitation("invitation_id")]);
        return view('frontend/beranda', $data);
    }

    public function theme()
    {
        $data['theme'] = $this->model->select_data("themes", "getResult");
        return view('frontend/theme', $data);
    }

    public function payment_finish(){
        return view('frontend/thankyou');
    }
}
