<?php

namespace App\Controllers;

class Event extends BaseController
{
    public function __construct()
    {
    }


    public function index()
    {

            $data['title'] = "Acara";
            $data['breadcrumb_title'] = "Pengaturan";
            $data['breadcrumb_subtitle'] = "Acara";
            $data['events'] = $this->model->select_data("events","getResult", ["user_id" => user("user_id"), "invitation_id" => invitation("invitation_id")]);
            return view('backend/users/event/index', $data);

        

    }

    public function create()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'event_name' => 'required',
                'date' => 'required',
                'time' => 'required',
                'location_name' => 'required',
                'location_address' => 'required',
                'latlng' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'user_id' => user("user_id"),
                    'invitation_id' => invitation("invitation_id"),
                    'event_name' => $this->request->getVar('event_name'),
                    'date' => $this->request->getVar('date'),
                    'time' => $this->request->getVar('time'),
                    'location_name' => $this->request->getVar('location_name'),
                    'location_address' => $this->request->getVar('location_address'),
                    'latlng' => $this->request->getVar('latlng'),
                ];
                if(limits("events")){
                $result = $this->model->insert("events", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Menambahkan Acara",
                    );

                    $redirect = redirect()->to('user/event');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/event/create');

                }
            }else{
                $notification = array(
                    "status" => "danger", "msg" => "Gagal, Kuota Acara sudah limit silahkan kontak admin",
                );
                $redirect = redirect()->to('user/guest');
            }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Acara";
            $data['breadcrumb_title'] = "Acara";
            $data['breadcrumb_subtitle'] = "Buah Acara";
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/event/create', $data);

        }

    }

    public function update($id = null)
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'event_name' => 'required',
                'date' => 'required',
                'time' => 'required',
                'location_name' => 'required',
                'location_address' => 'required',
                'latlng' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'event_name' => $this->request->getVar('event_name'),
                    'date' => $this->request->getVar('date'),
                    'time' => $this->request->getVar('time'),
                    'location_name' => $this->request->getVar('location_name'),
                    'location_address' => $this->request->getVar('location_address'),
                    'latlng' => $this->request->getVar('latlng'),
                ];

                $result = $this->model->update("events", $data, ["id" => decrypt($id)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah Acara",
                    );

                    $redirect = redirect()->to('user/event');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/event');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Acara";
            $data['breadcrumb_title'] = "Acara";
            $data['breadcrumb_subtitle'] = "Ubah Acara";
            $data['data'] = $this->model->select_data("events", "getRow", ["id" => decrypt($id)]);
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/event/edit', $data);

        }

    }

    public function delete($id = null)
    {
        $data = $this->model->select_data("events", "getRow", ["id" => decrypt($id)]);
        if ($data) {
            $this->model->delete("events", ["id" => decrypt($id)]);
            $notification = array(
                "status" => "success", "msg" => "Acara berhasil dihapus.",
            );
            $redirect = redirect()->to('user/event');

        } else {
            $notification = array(
                "status" => "danger", "msg" => "Gagal, Data acara gagal dihapus",
            );
            $redirect = redirect()->to('user/event');
        }
        session()->setFlashdata("notification", $notification);
        return $redirect;
    }
   
}
