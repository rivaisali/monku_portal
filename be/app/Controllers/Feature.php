<?php

namespace App\Controllers;

class Feature extends BaseController
{
    public function __construct()
    {
    }


    public function index()
    {

            $data['title'] = "Fitur";
            $data['breadcrumb_title'] = "Master";
            $data['breadcrumb_subtitle'] = "Fitur";
            $data['features'] = $this->model->select_data("features","getResult");
            return view('backend/admin/feature/index', $data);

        

    }




    public function create()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'feature' => 'required',
                'qty' => 'required',
                'category' => 'required',
                'unit' => 'required',
                'price' => 'required',

            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'feature' => $this->request->getVar('feature'),
                    'qty' => $this->request->getVar('qty'),
                    'category' => $this->request->getVar('category'),
                    'unit' => $this->request->getVar('unit'),
                    'price' => $this->request->getVar('price'),
                ];
                $result = $this->model->insert("features", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Menambahkan Fitur",
                    );

                    $redirect = redirect()->to('admin/feature');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('admin/feature/create');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Fitur";
            $data['breadcrumb_title'] = "Fitur";
            $data['breadcrumb_subtitle'] = "Tambah Fitur";
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/admin/feature/create', $data);

        }

    }



    public function update($id = null)
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'title' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'title' => $this->request->getVar('title'),
                    'description' => $this->request->getVar('description'),
                ];

                $result = $this->model->update("groups", $data, ["id" => decrypt($id)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah Grup",
                    );

                    $redirect = redirect()->to('user/group');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/group');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Grup";
            $data['breadcrumb_title'] = "Grup";
            $data['breadcrumb_subtitle'] = "Ubah Grup";
            $data['data'] = $this->model->select_data("groups", "getRow", ["id" => decrypt($id)]);
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/group/edit', $data);

        }

    }


    public function delete($id = null)
    {
        $data = $this->model->select_data("features", "getRow", ["id" => decrypt($id)]);
        if ($data) {
            $this->model->delete("features", ["id" => decrypt($id)]);
            $notification = array(
                "status" => "success", "msg" => "Fitur berhasil dihapus.",
            );
            $redirect = redirect()->to('admin/feature');

        } else {
            $notification = array(
                "status" => "danger", "msg" => "Gagal, Data Fitur gagal dihapus",
            );
            $redirect = redirect()->to('admin/feature');
        }
        session()->setFlashdata("notification", $notification);
        return $redirect;
    }

   
}
