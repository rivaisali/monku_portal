<?php

namespace App\Controllers;
use Ramsey\Uuid\Uuid;

class Invitation extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
       
    }

    public function start()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'title' => 'required',
                'date' => 'required',
                'user_type' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $uuid = Uuid::uuid4();

                $data = [
                    'uuid' => $uuid,
                    'user_id' => user("user_id"),
                    'date' => $this->request->getVar('date'),
                    'title' => $this->request->getVar('title'),
                    'user_type' => $this->request->getVar('user_type'),
                ];

                $result = $this->model->insert("invitations", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Silahkan Lanjutkan",
                    );

                    $redirect = redirect()->to('user/step/two/' . encrypt('step1') . '/' . encrypt($uuid));
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/invitation/create');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['validation'] = \Config\Services::validation();
            return view('backend/users/start/step_one', $data);

        }

    }

    public function step_two($step = null, $uuid = null)
    {
        if ($this->request->getVar('submit')) {

            $rules = [
                'domain' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $data = [
                    'full_domain' => $this->request->getVar('domain').".monku.id",
                    'acronym_domain' => $this->request->getVar('domain'),
                    'step_status' => "step2",
                ];

                $result = $this->model->update("invitations", $data, ["uuid" => decrypt($uuid)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Silahkan Lanjutkan",
                    );

                    $redirect = redirect()->to('user/step/three/' . encrypt('step2') . '/' . $uuid);
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/step/two/'.$step."/".$uuid);

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['validation'] = \Config\Services::validation();
             $result = $this->model->select_data("invitations", "getRow", ["step_status" => decrypt($step), "uuid" => decrypt($uuid)]);
             if($result){
                return view('backend/users/start/step_two', $data);
             }else{
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
             }
     

        }

    }
    public function step_three($step = null, $uuid = null)
    {
        if ($this->request->getVar('submit')) {

            $rules = [
                'package_id' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $data = [
                    'package_id' => $this->request->getVar('package_id'),
                    'step_status' => "step3",
                ];

                $result = $this->model->update("invitations", $data, ["uuid" => decrypt($uuid)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Silahkan Lanjutkan",
                    );

                    $redirect = redirect()->to('user/step/four/' . encrypt('step3') . '/' . $uuid);
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/step/three/'.$step."/".$uuid);

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $result = $this->model->select_data("invitations", "getRow", ["uuid" => decrypt($uuid), "step_status" => decrypt($step)]);
             if($result){
                $data['validation'] = \Config\Services::validation();
                

                $data['package'] = $this->model->select_data("packages", "getResult");
                $data['feature'] = $this->model->select_data_join("*", "feature_package", "getResult", [
                    [
                        "table" => "features",
                        "cond" => "features.id = feature_package.feature_id",
                        "type" => "",
                    ],
                    [
                        "table" => "packages",
                        "cond" => "packages.id = feature_package.package_id",
                        "type" => "",
                    ],
            
                ],
                    ["features.status" => "1"]);
                return view('backend/users/start/step_three', $data);
             }else{
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
             }
     

        }

    }

    public function step_four($step = null, $uuid = null)
    {
        if ($this->request->getVar('submit')) {

            $rules = [
                'theme_id' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $db = db_connect();
                $db->transStart();
                $data = [
                    'theme_id' => $this->request->getVar('theme_id'),
                    'step_status' => "step4",
                ];

                $this->model->update("invitations", $data, ["uuid" => decrypt($uuid)]);
                $invitation = $this->model->select_data("invitations", "getRow", ["uuid" => decrypt($uuid)]);
                $package = $this->model->select_data("packages", "getRow", ["id" => $invitation->package_id]);
                $total = $package->price - (($package->price * $package->discount) / 100);

                $year = date('Y');
                $date = date('Ymd');
                $count = $this->model->select_data_count("transactions", "getRow",
                    ["YEAR(date)" => $year]);
                $num_padded = sprintf("%03d", $count->count + 1);
                $transaction_number = "MONKU" . $date . $num_padded;

                $transaction = [
                    'user_id' => $invitation->user_id,
                    'invitation_id' => $invitation->id,
                    'package_id' => $invitation->package_id,
                    'transaction_number' => $transaction_number,
                    'date' => date('Y-m-d H:i:s'),
                    'pay_limit' => date('Y-m-d H:i:s',strtotime("+3 day")),
                    'total' =>  $total
                ];

                $result = $this->model->insert("transactions", $transaction);

                if ($result) {
                    $db->transComplete();
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Silahkan Lanjutkan",
                    );

                    $redirect = redirect()->to('user/payment/'. $uuid);
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/step/four/'.$step."/".$uuid);

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $result = $this->model->select_data("invitations", "getRow", ["uuid" => decrypt($uuid), "step_status" => decrypt($step)]);
             if($result){
                $data['validation'] = \Config\Services::validation();
                
                $data['theme'] = $this->model->select_data("themes", "getResult", false, ["package" => $result->package_id]);
                    // $db = db_connect();
                    // echo  $db->getLastQuery();
               return view('backend/users/start/step_four', $data);
             }else{
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
             }
     

        }

    }

    public function payment($uuid = null)
    {
        
            $result = $this->model->select_data("invitations", "getRow", ["uuid" => decrypt($uuid)]);
             if($result){
                $data['data'] = $this->model->select_data_join("*, transactions.date as transaction_date", "transactions", "getRow", [
                    [
                        "table" => "invitations",
                        "cond" => "transactions.invitation_id = invitations.id",
                        "type" => "",
                    ],
                    [
                        "table" => "packages",
                        "cond" => "transactions.package_id = packages.id",
                        "type" => "",
                    ],
                    [
                        "table" => "users",
                        "cond" => "transactions.user_id = users.id",
                        "type" => "",
                    ]
                ],
                    ["invitations.uuid" => decrypt($uuid)]);
                $data['validation'] = \Config\Services::validation();
               return view('backend/users/start/payment', $data);
             }else{
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
             }
     

        

    }

    public function create_template_message()
    {
        if ($this->request->getVar('submit')) {

            $rules = [
                'theme_id' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $data = [
                    'status' => "1",
                ];
                $invitation = $this->model->select_data("invitations", "getRow", ["uuid" => decrypt($uuid)]);
                $result = $this->model->update("transactions", $data, ["invitation_id" => $invitation->id]);

                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Silahkan Lanjutkan",
                    );

                    $redirect = redirect()->to('user');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/payment/'. $uuid);

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Template Pesan";
            $data['breadcrumb_title'] = "Template";
            $data['breadcrumb_subtitle'] = "Template Pesan";
            $data['example'] = $this->default_message();

            $result = $this->model->select_data("invitations", "getRow", ["user_id" => user("user_id")]);
            $data['validation'] = \Config\Services::validation();
            return view('backend/users/invitation/template_message', $data);
             }

        }

        public function default_message(){
                $pesan = "*Dear [[nama_tamu]]*
                Atas rahmat Tuhan Yang Maha Esa, mohon doa restu atas [[judul_undangan]] kami:
                
                *[[nama_pengantin_pria]] & [[nama_pengantin_wanita]]*
                
                Kami bermaksud mengundang bapak/ibu/saudara(i) pada acara [[judul_undangan]] kami yang akan dilaksanakan pada:\n
                
                [[acara]]";
                
                $pesan.="\n*Salam*
                *[[nama_pengantin_pria]] & [[nama_pengantin_wanita]]*
                
                ========================
                *Kode Undangan Anda: [[kode_tamu]]*
                *Dapatkan petunjuk arah & konfirmasi kedatangan anda, serta berikan pesan/doa terbaik anda melalui link berikut:* 
                
                https://.toduwo.id/[[kode_tamu]]
                
                *Pesan ini dikirim melalui toduwo.in*
                Made with somewhere in the world";
                return $pesan;
        
        }
}
