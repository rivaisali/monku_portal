<?php

namespace App\Controllers;

class Guest extends BaseController
{
    public function __construct()
    {
    }


    public function index()
    {

            $data['title'] = "Tamu";
            $data['breadcrumb_title'] = "Pengaturan";
            $data['breadcrumb_subtitle'] = "Tamu";
             $data['guests'] = $this->model->select_data_join("*, guests.id as guest_id", "guests", "getResult", [
                [
                    "table" => "groups",
                    "cond" => "groups.id = guests.group_id",
                    "type" => "",
                ],
               
            ],
            ["guests.user_id" => user("user_id"), "guests.invitation_id" => invitation("invitation_id")]);
            return view('backend/users/guest/index', $data);

        

    }

    public function send()
    {

            $data['title'] = "Kirim Undangan";
            $data['breadcrumb_title'] = "Pengaturan";
            $data['breadcrumb_subtitle'] = "Kirim Undangan";
            $data['invitation'] = $this->model->select_data('invitations', 'getRow', ["id" => invitation("invitation_id")]);
             $data['guests'] = $this->model->select_data_join("*, guests.id as guest_id", "guests", "getResult", [
                [
                    "table" => "groups",
                    "cond" => "groups.id = guests.group_id",
                    "type" => "",
                ],
                [
                    "table" => "invitations",
                    "cond" => "invitations.id = guests.invitation_id",
                    "type" => "",
                ],
               
            ],
            ["guests.user_id" => user("user_id"), "guests.invitation_id" => invitation("invitation_id")]);

            $data['event'] = $this->model->select_data('events', 'getResult', ["invitation_id" => invitation("invitation_id")]);
            return view('backend/users/guest/view', $data);

        

    }

    public function create()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'group_id' => 'required',
                'full_name' => 'required',
                'phone' => 'required',
                'guest_number' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'group_id' => $this->request->getVar('group_id'),
                    'user_id' => user("user_id"),
                    'invitation_id' => invitation("invitation_id"),
                    'code' => generateGuestCode("10"),
                    'full_name' => $this->request->getVar('full_name'),
                    'phone' => $this->request->getVar('phone'),
                    'guest_number' => $this->request->getVar('guest_number'),
                    'email' => $this->request->getVar('email'),
                ];

                if(limits("guests")){
                $result = $this->model->insert("guests", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Menambahkan Tamu",
                    );

                    $redirect = redirect()->to('user/guest');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/guest/create');

                }
            }else{
                $notification = array(
                    "status" => "danger", "msg" => "Gagal, Kuota Tamu sudah limit silahkan kontak admin",
                );
                $redirect = redirect()->to('user/guest');
            }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Tamu";
            $data['breadcrumb_title'] = "Tamu";
            $data['breadcrumb_subtitle'] = "Tambah Tamu";
            $data['groups'] = $this->model->select_data("groups", "getResult", ["user_id" => user("user_id"), "invitation_id" => invitation("invitation_id")]);
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/guest/create', $data);

        }

    }

    public function update($id = null)
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'group_id' => 'required',
                'full_name' => 'required',
                'phone' => 'required',
                'guest_number' => 'required'
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'group_id' => $this->request->getVar('group_id'),
                    'full_name' => $this->request->getVar('full_name'),
                    'phone' => $this->request->getVar('phone'),
                    'guest_number' => $this->request->getVar('guest_number'),
                    'email' => $this->request->getVar('email'),
                ];

                $result = $this->model->update("guests", $data, ["id" => decrypt($id)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah Tamu",
                    );

                    $redirect = redirect()->to('user/guest');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/guest');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Tamu";
            $data['breadcrumb_title'] = "Tamu";
            $data['breadcrumb_subtitle'] = "Ubah Tamu";
            $data['data'] = $this->model->select_data("guests", "getRow", ["id" => decrypt($id)]);
            $data['groups'] = $this->model->select_data("groups", "getResult", ["user_id" => user("user_id"), "invitation_id" => invitation("invitation_id")]);
           
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/guest/edit', $data);

        }

    }

    public function delete($id = null)
    {
        $data = $this->model->select_data("guests", "getRow", ["id" => decrypt($id)]);
        if ($data) {
            $this->model->delete("guests", ["id" => decrypt($id)]);
            $notification = array(
                "status" => "success", "msg" => "Tamu berhasil dihapus.",
            );
            $redirect = redirect()->to('user/guest');

        } else {
            $notification = array(
                "status" => "danger", "msg" => "Gagal, Data acara gagal dihapus",
            );
            $redirect = redirect()->to('user/guest');
        }
        session()->setFlashdata("notification", $notification);
        return $redirect;
    }
   
}
