<?php

namespace App\Controllers;

class Package extends BaseController
{
    public function __construct()
    {
    }


    public function index()
    {

            $data['title'] = "Paket";
            $data['breadcrumb_title'] = "Master";
            $data['breadcrumb_subtitle'] = "Paket";
            $data['packages'] = $this->model->select_data("packages","getResult");
            return view('backend/admin/package/index', $data);

        

    }


    public function detail($id)
    {
            $data['title'] = "Paket";
            $data['breadcrumb_title'] = "Master";
            $data['breadcrumb_subtitle'] = "Detail Paket";
            $data['uri'] = service('uri');
            $data['packages'] = $this->model->select_data_join("*,feature_package.id as feature_package_id", "feature_package", "getResult", [
                [
                    "table" => "packages",
                    "cond" => "packages.id = feature_package.package_id",
                    "type" => "",
                ],
                [
                    "table" => "features",
                    "cond" => "features.id = feature_package.feature_id",
                    "type" => "",
                ],
               
            ],["feature_package.package_id" => decrypt($id)]);
            return view('backend/admin/package/detail/index', $data);

        

    }


    public function create()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'title' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'user_id' => user("user_id"),
                    'invitation_id' => invitation("invitation_id"),
                    'title' => $this->request->getVar('title'),
                    'description' => $this->request->getVar('description'),
                ];
                if(limits("groups")){
                $result = $this->model->insert("groups", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Menambahkan Grup",
                    );

                    $redirect = redirect()->to('user/group');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/group/create');

                }
            }else{
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Kuota Grup sudah limit silahkan kontak admin",
                    );
                    $redirect = redirect()->to('user/guest');
                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Grup";
            $data['breadcrumb_title'] = "Grup";
            $data['breadcrumb_subtitle'] = "Tambah Grup";
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/group/create', $data);

        }

    }


    public function detail_create()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'package_id' => 'required',
                'feature_id' => 'required',
                'quota' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'package_id' => decrypt($this->request->getVar('package_id')),
                    'feature_id' => $this->request->getVar('feature_id'),
                    'quota' => $this->request->getVar('quota'),
                ];
                $result = $this->model->insert("feature_package", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Menambahkan Fitur",
                    );

                    $redirect = redirect()->to('admin/package/detail/'.$this->request->getVar('package_id'));
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('admin/package/detail/'.$this->request->getVar('package_id'));

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Fitur";
            $data['breadcrumb_title'] = "Fitur";
            $data['breadcrumb_subtitle'] = "Tambah Fitur";
            $data['uri'] = service('uri');
            $data['validation'] = \Config\Services::validation();
            $data['features'] = $this->model->select_data("features", "getResult");
            
            return view('backend/admin/package/detail/create', $data);

        }

    }

    public function update($id = null)
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'title' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'title' => $this->request->getVar('title'),
                    'description' => $this->request->getVar('description'),
                ];

                $result = $this->model->update("groups", $data, ["id" => decrypt($id)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah Grup",
                    );

                    $redirect = redirect()->to('user/group');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/group');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Grup";
            $data['breadcrumb_title'] = "Grup";
            $data['breadcrumb_subtitle'] = "Ubah Grup";
            $data['data'] = $this->model->select_data("groups", "getRow", ["id" => decrypt($id)]);
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/group/edit', $data);

        }

    }

    public function detail_update($id = null)
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'feature_id' => 'required',
                'quota' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'feature_id' => $this->request->getVar('feature_id'),
                    'quota' => $this->request->getVar('quota'),
                ];

                $result = $this->model->update("feature_package", $data, ["id" => decrypt($id)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah Fitur Paket",
                    );

                    $redirect = redirect()->to('admin/package/detail/'.$this->request->getVar('package_id'));
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('admin/package/detail/'.$this->request->getVar('package_id'));
                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Package Detail";
            $data['breadcrumb_title'] = "Package Detail";
            $data['breadcrumb_subtitle'] = "Ubah Package Detail";
            $data['features'] = $this->model->select_data("features", "getResult");
            $data['uri'] = service('uri');
            $data['data'] = $this->model->select_data("feature_package", "getRow", ["id" => decrypt($id)]);
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/admin/package/detail/edit', $data);


        }

    }

    public function delete($id = null)
    {
        $data = $this->model->select_data("groups", "getRow", ["id" => decrypt($id)]);
        if ($data) {
            $this->model->delete("groups", ["id" => decrypt($id)]);
            $notification = array(
                "status" => "success", "msg" => "Grup berhasil dihapus.",
            );
            $redirect = redirect()->to('user/group');

        } else {
            $notification = array(
                "status" => "danger", "msg" => "Gagal, Data acara gagal dihapus",
            );
            $redirect = redirect()->to('user/group');
        }
        session()->setFlashdata("notification", $notification);
        return $redirect;
    }

    public function detail_delete($id = null)
    {
        $data = $this->model->select_data("feature_package", "getRow", ["id" => decrypt($id)]);
        if ($data) {
            $this->model->delete("feature_package", ["id" => decrypt($id)]);
            $notification = array(
                "status" => "success", "msg" => "Fitur berhasil dihapus.",
            );
            $redirect = redirect()->to('admin/package/detail/'.encrypt($data->package_id));

        } else {
            $notification = array(
                "status" => "danger", "msg" => "Gagal, Data tidak ditemukan",
            );
            $redirect = redirect()->to('admin/package');
        }
        session()->setFlashdata("notification", $notification);
        return $redirect;
    }
   
}
