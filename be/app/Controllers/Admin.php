<?php

namespace App\Controllers;

class Admin extends BaseController
{

    public function __construct()
    {
         
        
    }

    public function index()
    {
    
        $data['count_users'] = $this->model->select_data_count("users", "getRow", ["role" => "user"]);
        $data['data'] = $this->model->select_data_join("*, transactions.date as transaction_date,transactions.status as transaction_status
        ,transactions.id as transactions_id", "transactions", "getResult", [

            [
                "table" => "packages",
                "cond" => "transactions.package_id = packages.id",
                "type" => "",
            ],
            [
                "table" => "users",
                "cond" => "transactions.user_id = users.id",
                "type" => "",
            ]
        ]);
        return view('backend/admin/beranda', $data);
    }

    
}
