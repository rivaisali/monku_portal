<?php

namespace App\Controllers;

class Payment extends BaseController
{

    public function index($id = null)
    {
       // \Midtrans\Config::$serverKey = 'SB-Mid-server-HYFjxiBUWZOr9YP3G3SEtNXD';
        \Midtrans\Config::$serverKey = 'Mid-server-llkNRgKNLK35SmIFdR6MHq4g';

        // non-relevant function only used for demo/example purpose

        // Uncomment for production environment
        \Midtrans\Config::$isProduction = true;

        // Uncomment to enable sanitization
        // Config::$isSanitized = true;

        // Uncomment to enable 3D-Secure
        // Config::$is3ds = true;

        $data = $this->model->select_data_join("*,invitations.id as invitation_id,transactions.id as transaction_id", "transactions", "getRow", [
            [
                "table" => "invitations",
                "cond" => "invitations.id = transactions.invitation_id",
                "type" => "",
            ],
            [
                "table" => "users",
                "cond" => "users.id = transactions.user_id",
                "type" => "",
            ],

        ], ["transactions.invitation_id" => decrypt($id)]);
        if($data){
// Required
        $orderID = $data->invitation_id."#".$data->transaction_id."#".time();
        $transaction_details = array(
            'order_id' => encrypt($orderID),
            'gross_amount' => $data->total, // no decimal allowed for creditcard
        );

// Optional
        $item_details = array(
            'id' => $data->transaction_number,
            'price' => $data->total,
            'quantity' => 1,
            'name' => "Undangan Digital (" . $data->full_domain . ")",
        );

// Optional
        $customer_details = array(
            'first_name' => $data->full_name,
            'last_name' => null,
            'email' => $data->email,
            'phone' => $data->phone,
        );

// Fill SNAP API parameter
        $params = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => array($item_details),
        );

        try {
            // Get Snap Payment Page URL
            $paymentUrl = \Midtrans\Snap::createTransaction($params)->redirect_url;

            // Redirect to Snap Payment Page
            // echo $paymentUrl;
            // header('Location: ' . $paymentUrl);
            return redirect()->to($paymentUrl);
        } catch (\Exception$e) {
            echo $e->getMessage();
        }
       // sendInvoice($data->email, $data->full_name, $paymentUrl);
    }else{
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    }

    public function notification()
    {
        \Midtrans\Config::$serverKey = 'SB-Mid-server-HYFjxiBUWZOr9YP3G3SEtNXD';
        $notif = new \Midtrans\Notification();
 
        $data = [
                    'transaction_id' => $notif->transaction_id,
                    'order_id' => $notif->order_id,
                    'transaction_time' => $notif->transaction_time,
                    'payment_type' => $notif->payment_type,
                    'gross_amount' => $notif->gross_amount,
                    'transaction_status' => $notif->transaction_status,
                    'fraud_status' => $notif->fraud_status,
                    'status_code' => $notif->status_code,
                ];
                $payment = $this->model->select_data_count("payments","getRow", ["order_id" => $notif->order_id])->count;
                $order_id = explode("#",decrypt($notif->order_id));
                if($payment > 0){
                    $update = $this->model->update("payments", $data, ["order_id" => $notif->order_id]);
                    if($notif->transaction_status==="settlement"){   
                    $transaction = $this->model->update("transactions", ["status" => "2"], ["id" => $order_id[1]]);
                    $invitation = $this->model->update("invitations", ["status" => "1"], ["id" => $order_id[0]]);
                    }
                }else{
                    $insert = $this->model->insert("payments", $data);
                    $transaction = $this->model->update("transactions", ["status" => "1"], ["id" => $order_id[1]]);
                }

    }
}
