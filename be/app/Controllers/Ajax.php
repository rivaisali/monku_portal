<?php

namespace App\Controllers;

class Ajax extends BaseController
{
    public function __construct()
    {
    }


    public function list_features()
    {
        $response = [];
		$package_id = $this->request->getVar('package_id');
        $result = $this->model->select_data_join("*", "feature_package", "getResult", [
            [
                "table" => "features",
                "cond" => "features.id = feature_package.feature_id",
                "type" => "",
            ],
            [
                "table" => "packages",
                "cond" => "packages.id = feature_package.package_id",
                "type" => "",
            ],
    
        ],
            ["features.status" => "1", "package_id" => $package_id]);

		if ($result) {
			$response = [
				"status" => true,
                "data" => $result,
			];
		} else {
			$response = [
				"status" => false,
                "data" => [],
			];
		}
		echo json_encode($response);
    }

    public function check_domain()
    {
        $response = [];
		$domain = $this->request->getVar('domain');
        $result = $this->model->select_data("invitations", "getRow", ["acronym_domain" => $domain]);
		if ($result) {
			$response = [
				"status" => true,
                "label" => "btn btn-danger",
                "msg" => "Domain Tidak Tersedia"
			];
		} else {
			$response = [
				"status" => false,
                "label" => "btn btn-success",
                "msg" => "Domain Tersedia"
			];
		}
		echo json_encode($response);
    }
}
