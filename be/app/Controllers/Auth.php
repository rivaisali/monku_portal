<?php

namespace App\Controllers;

use App\Models\AuthModel;

class Auth extends BaseController
{
    protected $authModel;

    public function __construct()
    {
        helper('cookie');
        $this->authModel = new AuthModel();

    }

    public function index()
    {
        if (!session()->has('user')) {
            $data = [];
            $data['title'] = "Login";
            return view("backend/login", $data);
        } else {
           $role = user("role");
            return redirect()->to('/'.$role);
            
        }
    }

    

    public function login()
    {
        $output = array('error' => false);
        $username = $this->request->getVar('username');
        $pass = $this->request->getVar('password');
        if ($username == "" || $pass == "") {
            $output['error'] = true;
            $output['message'] = 'Form tidak boleh kosong
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';
        } else {
            $throttler = \Config\Services::throttler();
            $allowed = $throttler->check('login', 4, 170);
            if ($allowed) {
                $cek_username = $this->authModel->cekUsername($username);
                if (!empty($cek_username)) {
                    $password = password_verify($this->request->getVar("password"), $cek_username['password']);
                    if ($password) {
                        $data = $cek_username;
                        if ($data['status'] == "0") {
                            $output['error'] = true;
                            $output['message'] = 'Akun anda belum di Aktifkan, cek Email anda!
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';
                        } else if ($data['status'] == "2") {
                            $output['error'] = true;
                            $output['message'] = '<p><i class="fa fa-bell"></i>Akun anda belum di Nonaktifkan</p>
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';
                        } else {
                            $output['error'] = false;
                            $update = array(
                                "last_logged" => date("Y-m-d H:i:s"),
                            );
                            $this->authModel->updateLastLogin($update, $data['id']);
                            $session_user = [
                                "user_id" => $data["id"],
                                "full_name" => $data["full_name"],
                                "email" => $data["email"],
                                "phone" => $data["phone"],
                                "role" => $data["role"],
                                "status" => $data["status"],
                            ];
                            $this->session->set('user', $session_user);
                            if ($this->request->getVar('remember')) {
                                set_cookie('login_id', $this->request->getVar('username'), time() + (10 * 365 * 24 * 60 * 60));
                                set_cookie('login_password', $this->request->getVar('password'), time() + (10 * 365 * 24 * 60 * 60));
                            } else {
                                set_cookie('login_id', '');
                                set_cookie('login_password', '');
                            }
                            $output['message'] = 'Login Berhasil. Tunggu sebentar...
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';

                        }
                    } else {
                        $output['error'] = true;
                        $output['message'] = 'Username atau Password Salah!
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';
                    }
                } else {
                    $output['error'] = true;
                    $output['message'] = 'Email atau Username Belum terdaftar !
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';
                }
            } else {
                $output['error'] = true;
                $output['message'] = 'Terlalu banyak login silahkan coba lagi 1 menit kemudian!
                      <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close"></button>';

            }
        }
        echo json_encode($output);
    }

    public function signup()
    {
        if ($this->request->getVar('submit')) {
            $rules = [
                'full_name' => 'required',
                'phone' => 'required',
                'email' => 'required|valid_email|is_unique[users.email]',
                'password' => 'required|min_length[6]',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $data = [
                    "full_name" => $this->request->getVar('full_name'),
                    "phone" => strtoupper($this->request->getVar('phone')),
                    "email" => $this->request->getVar('email'),
                    "password" => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),

                ];

                $signup = $this->model->insert("users", $data);

                if ($signup) {
                    $token = $this->activation_tokens($this->request->getVar('email'));
                    sendEmail($this->request->getVar('email'), $this->request->getVar('full_name'), $token);

                    $notification = array(
                        "status" => "success", "msg" => "Anda Berhasil melakukan registrasi, silahkan cek email anda.",
                    );

                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal melakukan registrasi",
                    );

                }

                session()->setFlashdata('notification', $notification);
                return redirect()->to('/login');
            }
        } else {

            $data['validation'] = \Config\Services::validation();
            return view('backend/signup', $data);

        }

    }

    public function logout()
    {
        $notification = array(
            "status" => "success", "msg" => "Selamat Tinggal. Anda Berhasil Keluar",
        );

        $this->session->setFlashdata("notification", $notification);
        $this->session->destroy();
        return redirect()->to('/');
    }

    public function activation_tokens($email)
    {

        $token = substr(sha1(rand()), 0, 30);
        $date = date('Y-m-d');

        $data = [
            'token' => $token,
            'email' => $email,
            'created' => $date,
        ];

        $result = $this->model->insert("activation_tokens", $data);
        if ($result) {
            $token = $this->base64url_encode($token . $email);
            return $token;
        } else {
            echo "token tidak valid";

        }

    }

    public function activation($token = null)
    {
        $token = $this->base64url_decode($token);

        $validation = $this->isTokenValid($token);

        if ($validation == false) {
            $notification = array(
                "status" => "danger", "msg" => "Token anda tidak valid!",
            );
        } else {
            $check = $this->model->select_data_count("users", "getRow", ["email" => $validation->email, "status" => "1"]);
            if ($check->count > 0) {
                $notification = array(
                    "status" => "warning", "msg" => "Akun anda sudah Aktif, Silahkan melakukan login",
                );
            } else {
                $this->model->update("users", ["status" => "1"], ["email" => $validation->email]);
                $notification = array(
                    "status" => "success", "msg" => "Akun anda berhasil di Aktivasi, Silahkan melakukan login",
                );
            }

        }

        session()->setFlashdata('notification', $notification);
        return redirect()->to('/login');

    }

    public function isTokenValid($token)
    {
        $tkn = substr($token, 0, 30);
        $uid = substr($token, 30);

        $user = $this->model->select_data("activation_tokens", "getRow", ["email" => $uid, "token" => $tkn]);

        if ($user) {
            $created = $user->created;
            $createdTS = strtotime($created);
            $today = date('Y-m-d');
            $todayTS = strtotime($today);

            if ($createdTS != $todayTS) {
                return false;
            }

            return $user;

        } else {
            return false;

        }

    }

    public function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');

    }

    public function base64url_decode($data)
    {

        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));

    }

}
