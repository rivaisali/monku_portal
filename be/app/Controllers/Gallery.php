<?php

namespace App\Controllers;

class Gallery extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
        $data['title'] = "Galeri";
        $data['breadcrumb_title'] = "Galeri";
        $data['breadcrumb_subtitle'] = "Tambah Galeri";
        $data['max_image'] = "6";
        $data['data'] = $this->model->select_data("gallery","getResult", ["user_id" => user("user_id"), "invitation_id" => invitation("invitation_id")]);
        return view('backend/users/gallery/index', $data);
    }

    public function create()
    {
        if(limits("gallery")){
        $file = $this->request->getFile('images');
        $fileName = $file->getRandomName();
        $data = [
            'title' => $this->request->getVar('title'),
            'user_id' => user("user_id"),
            'invitation_id' => invitation("invitation_id"),
            'image' => $fileName,
        ];
     
        $file->move('./uploads/gallery', $fileName);
        $result = $this->model->insert("gallery", $data);
    }
     
   }
}
