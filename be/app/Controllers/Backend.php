<?php

namespace App\Controllers;

class Backend extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
        return view('backend/users/start/step_one');
    }
}
