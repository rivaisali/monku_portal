<?php

namespace App\Controllers;

class Bridegroom extends BaseController
{
    public function __construct()
    {
    }


    public function index()
    {
        helper(['form', 'array']);

        if ($this->request->getVar('submit')==="save_groom") {

            $rules = [
                'groom_name' => 'required',
                'groom_nickname' => 'required',
                'groom_about' => 'required',
            ];

        $fileName = dot_array_search('imageGroom.name', $_FILES);

        if ($fileName != '') {
            $img = ['imageGroom' => 'uploaded[imageGroom]|max_size[imageGroom, 1024]|is_image[imageGroom]'];
            $rules = array_merge($rules, $img);    
        }

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $item = $this->model->select_data("invitations", "getRow", ["user_id" => user("user_id"), "id" => invitation("invitation_id")]);
                if ($item) {
                $data = [
                    'groom_name' => $this->request->getVar('groom_name'),
                    'groom_about' => $this->request->getVar('groom_about'),
                    'groom_ig' => $this->request->getVar('groom_ig'),
                    'groom_nickname' => $this->request->getVar('groom_nickname'),
                ];

                if ($fileName != '') {
                    $file = $this->request->getFile('imageGroom');
                    $fileName = $file->getRandomName();
                    if (!$file->isValid()) {
                        return $this->fail($file->getErrorString());
                    }
                    if ($item->groom_photo != "") {
                        unlink("./uploads/bridegroom/" . $item->groom_photo);
                    }

                    $file->move('./uploads/bridegroom', $fileName);
                    $data['groom_photo'] = $fileName;

                }

                $result = $this->model->update("invitations", $data, ["user_id" => user("user_id"), "id" => invitation("invitation_id")]);
                if ($result) {
 
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah data Calon Pengantin Pria",
                    );

                    $redirect = redirect()->to('user/bridegroom');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/bridegroom');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }
        }

        }else if ($this->request->getVar('submit')==="save_bride") {

            $rules = [
                'bride_name' => 'required',
                'bride_nickname' => 'required',
                'bride_about' => 'required',
            ];

        $fileName = dot_array_search('imageBride.name', $_FILES);

        if ($fileName != '') {
            $img = ['imageBride' => 'uploaded[imageBride]|max_size[imageBride, 1024]|is_image[imageBride]'];
            $rules = array_merge($rules, $img);
        }

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
                $item = $this->model->select_data("invitations", "getRow", ["user_id" => user("user_id"),"id" => invitation("invitation_id")]);
                if ($item) {
                $data = [
                    'bride_name' => $this->request->getVar('bride_name'),
                    'bride_about' => $this->request->getVar('bride_about'),
                    'bride_ig' => $this->request->getVar('bride_ig'),
                    'bride_nickname' => $this->request->getVar('bride_nickname'),
                ];

                if ($fileName != '') {
                    $file = $this->request->getFile('imageBride');
                    $fileName = $file->getRandomName();
                    if (!$file->isValid()) {
                        return $this->fail($file->getErrorString());
                    }
                    if ($item->bride_photo != "") {
                        unlink("./uploads/bridegroom/" . $item->bride_photo);
                    }

                    $file->move('./uploads/bridegroom', $fileName);
                    $data['bride_photo'] = $fileName;

                }

                $result = $this->model->update("invitations", $data, ["user_id" => user("user_id"), "id" => invitation("invitation_id")]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah data Calon Pengantin Pria",
                    );

                    $redirect = redirect()->to('user/bridegroom');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/bridegroom');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }
          }
        } else {
            $data['title'] = "Pengantin";
            $data['breadcrumb_title'] = "Pengaturan";
            $data['breadcrumb_subtitle'] = "Pengantin";
            $data['validation'] = \Config\Services::validation();
            $data['data'] = $this->model->select_data("invitations","getRow", ["user_id" => user("user_id"), "id" => invitation("invitation_id")]);
            return view('backend/users/bridegroom/index', $data);

        }

    }

    
}
