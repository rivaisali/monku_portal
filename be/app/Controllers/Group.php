<?php

namespace App\Controllers;

class Group extends BaseController
{
    public function __construct()
    {
    }


    public function index()
    {

            $data['title'] = "Grup Tamu";
            $data['breadcrumb_title'] = "Pengaturan";
            $data['breadcrumb_subtitle'] = "Grup Tamu";
            $data['guests'] = $this->model->select_data("groups","getResult", ["user_id" => user("user_id"), "invitation_id" => invitation("invitation_id")]);
            return view('backend/users/group/index', $data);

        

    }

    public function create()
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'title' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'user_id' => user("user_id"),
                    'invitation_id' => invitation("invitation_id"),
                    'title' => $this->request->getVar('title'),
                    'description' => $this->request->getVar('description'),
                ];
                if(limits("groups")){
                $result = $this->model->insert("groups", $data);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Menambahkan Grup",
                    );

                    $redirect = redirect()->to('user/group');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/group/create');

                }
            }else{
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Kuota Grup sudah limit silahkan kontak admin",
                    );
                    $redirect = redirect()->to('user/guest');
                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Grup";
            $data['breadcrumb_title'] = "Grup";
            $data['breadcrumb_subtitle'] = "Tambah Grup";
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/group/create', $data);

        }

    }

    public function update($id = null)
    {

        if ($this->request->getVar('submit')) {

            $rules = [
                'title' => 'required',
            ];

            if (!$this->validate($rules)) {
                return redirect()->back()->withInput();
            } else {
              
                $data = [
                    'title' => $this->request->getVar('title'),
                    'description' => $this->request->getVar('description'),
                ];

                $result = $this->model->update("groups", $data, ["id" => decrypt($id)]);
                if ($result) {
                    $notification = array(
                        "status" => "success", "msg" => "Berhasil, Mengubah Grup",
                    );

                    $redirect = redirect()->to('user/group');
                } else {
                    $notification = array(
                        "status" => "danger", "msg" => "Gagal, Periksa kembali data yang anda masukan.",
                    );
                    $redirect = redirect()->to('user/group');

                }
                session()->setFlashdata("notification", $notification);
                return $redirect;

            }

        } else {
            $data['title'] = "Grup";
            $data['breadcrumb_title'] = "Grup";
            $data['breadcrumb_subtitle'] = "Ubah Grup";
            $data['data'] = $this->model->select_data("groups", "getRow", ["id" => decrypt($id)]);
            $data['validation'] = \Config\Services::validation();
            
            
            return view('backend/users/group/edit', $data);

        }

    }

    public function delete($id = null)
    {
        $data = $this->model->select_data("groups", "getRow", ["id" => decrypt($id)]);
        if ($data) {
            $this->model->delete("groups", ["id" => decrypt($id)]);
            $notification = array(
                "status" => "success", "msg" => "Grup berhasil dihapus.",
            );
            $redirect = redirect()->to('user/group');

        } else {
            $notification = array(
                "status" => "danger", "msg" => "Gagal, Data acara gagal dihapus",
            );
            $redirect = redirect()->to('user/group');
        }
        session()->setFlashdata("notification", $notification);
        return $redirect;
    }
   
}
