<?php

namespace App\Controllers;

class User extends BaseController
{

    public function __construct()
    {
         
        
    }

    public function index()
    {
        

        $data['count_group'] = $this->model->select_data_count("groups", "getRow", ["user_id" => user("user_id")]);
        $data['count_guest'] = $this->model->select_data_count("guests", "getRow", ["user_id" => user("user_id")]);
        $data['count_event'] = $this->model->select_data_count("events", "getRow", ["user_id" => user("user_id")]);
        $data['data'] = $this->model->select_data_join("*, transactions.date as transaction_date,transactions.status as transaction_status
        ,invitations.status as invitation_status", "transactions", "getRow", [
            [
                "table" => "invitations",
                "cond" => "transactions.invitation_id = invitations.id",
                "type" => "",
            ],
            [
                "table" => "packages",
                "cond" => "transactions.package_id = packages.id",
                "type" => "",
            ],
            [
                "table" => "users",
                "cond" => "transactions.user_id = users.id",
                "type" => "",
            ]
        ],
            ["invitations.user_id" => user("user_id"), "invitations.id" => invitation("invitation_id")]);
        return view('backend/users/beranda', $data);
    }

    public function start()
    {
        if ($this->request->getVar('choose')) {
            $session_invitation = [
                "invitation_id" => decrypt($this->request->getVar('choose')),
            ];

            $session = session();

            $result = $this->model->select_data("invitations", "getRow",
                    ["status" => '1', "id" => decrypt($this->request->getVar('choose'))]);

            if($result) {
                $session->set('invitation', $session_invitation);

                $notification = array(
                    "status" => "success", "msg" => "Berhasil, silahkan kelola undangan anda.",
                );
                $redirect = redirect()->to('/user');
            }else{
                $notification = array(
                    "status" => "danger", "msg" => "Undangan anda belum aktif, silahkan lakukan pembayaran terlebih dahulu",
                );
                $redirect = redirect()->to('/user/start');
            }

           

            session()->setFlashdata('notification', $notification);
            return $redirect;
        } else {

            $data['data'] = $this->model->select_data("invitations", "getResult", ["user_id" => user("user_id")]);
            $data['validation'] = \Config\Services::validation();
            return view('backend/start', $data);
        }
    }

    public function setting()
    {
        

        $data['count_group'] = $this->model->select_data_count("groups", "getRow", ["user_id" => user("user_id")]);
        $data['count_guest'] = $this->model->select_data_count("guests", "getRow", ["user_id" => user("user_id")]);
        $data['count_event'] = $this->model->select_data_count("events", "getRow", ["user_id" => user("user_id")]);
        $data['data'] = $this->model->select_data_join("*, transactions.date as transaction_date,transactions.status as transaction_status
        ,invitations.status as invitation_status", "transactions", "getRow", [
            [
                "table" => "invitations",
                "cond" => "transactions.invitation_id = invitations.id",
                "type" => "",
            ],
            [
                "table" => "packages",
                "cond" => "transactions.package_id = packages.id",
                "type" => "",
            ],
            [
                "table" => "users",
                "cond" => "transactions.user_id = users.id",
                "type" => "",
            ]
        ],
            ["invitations.user_id" => user("user_id"), "invitations.id" => invitation("invitation_id")]);
        return view('backend/users/setting', $data);
    }
}
