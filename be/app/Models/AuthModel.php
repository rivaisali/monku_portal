<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $allowedFields = ['full_name', 'user_type_id', 'user_type_id', 'password', 'phone', 'status', 'role', 'last_logged'];

    public function cekUsername($username)
    {
        return $this->orWhere('email', $username)->first();
    }

    public function updateLastLogin($data, $id)
    {
        $this->set($data);
        $this->where('id', $id);
        $this->update();
    }
}
