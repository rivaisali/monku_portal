<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class UserFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        if (session()->has('user')) {
            $model = new \App\Models\CustomModel();
            $user = session()->get("user")["user_id"];
            $res = $model->select_data("invitations", "getRow", ["user_id" => $user]);
            if (!$res) {
                return redirect()->to('/user/invitation/create');
            } else {
                if (!session()->has('invitation')) {
                    return redirect()->to('/user/start');
                }
            }

        } else {
            return redirect()->to('/login');
        }

    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here

    }
}
