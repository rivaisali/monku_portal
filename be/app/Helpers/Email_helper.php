<?php

use Config\App;

function sendEmail($to, $name, $token = null)
{
    $template = '<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Aktivasi Akun - Monku Indonesia</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="Platform Undangan Digital Terbaik" />
            <meta name="keywords" content="Undangan, Monku, Digital, Dashboard, Modern, Classic" />
            <meta name="author" content="PT Arajang Digital Indonesia" />
            <meta name="Version" content="v3.8.0" />
            <!-- favicon -->
            <link rel="shortcut icon" href="assets/images/favicon.ico">
            <!-- Font -->
            <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&display=swap" rel="stylesheet">
        </head>
        <body style="font-family: Nunito, sans-serif; font-size: 15px; font-weight: 400;">
    
            <!-- Hero Start -->
            <div style="margin-top: 50px;">
                <table cellpadding="0" cellspacing="0" style="font-family: Nunito, sans-serif; font-size: 15px; font-weight: 400; max-width: 600px; border: none; margin: 0 auto; border-radius: 6px; overflow: hidden; background-color: #fff; box-shadow: 0 0 3px rgba(60, 72, 88, 0.15);">
                    <thead>
                        <tr style="background-color: #F2467F; padding: 3px 0; border: none; line-height: 68px; text-align: center; color: #fff; font-size: 24px; letter-spacing: 1px;">
                            <th scope="col"><img src="'.base_url().'/assets/frontend/images/logo.png" height="32" alt=""></th>
                        </tr>
                    </thead>
        
                    <tbody>
                        <tr>
                            <td style="padding: 48px 24px 0; color: #161c2d; font-size: 18px; font-weight: 600;">
                                Hay, '.$name.'
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 15px 24px 15px; color: #8492a6;">
                            Terima kasih telah membuat akun Monku. Untuk melanjutkan, harap konfirmasikan alamat email Anda dengan mengklik tombol di bawah ini:
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 15px 24px;">
                                <a href="'.base_url('aktivasi').'/'.$token.'" style="padding: 8px 20px; outline: none; text-decoration: none; font-size: 16px; letter-spacing: 0.5px; transition: all 0.3s; font-weight: 600; border-radius: 6px; background: linear-gradient(45deg, #F957A6, #F2467F) !important; color: #ffffff;">
                                Konfirmasi</a>
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 15px 24px 0; color: #8492a6;">
                            Tautan ini akan aktif selama 12 Jam sejak email ini dikirim.
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 15px 24px 15px; color: #8492a6;">
                                Monku Indonesia <br> Support Team
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 16px 8px; color: #8492a6; background-color: #f8f9fc; text-align: center;">
                                © <script>document.write(new Date().getFullYear())</script> Monku Indonesia.
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Hero End -->
        </body>
    </html>';

    email_config("Registrasi Hak Akses Undangan Digital", $template, $to);
}

function sendInvoice($to, $name, $url)
{
    $template = '<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Aktivasi Akun - Monku Indonesia</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="Platform Undangan Digital Terbaik" />
            <meta name="keywords" content="Undangan, Monku, Digital, Dashboard, Modern, Classic" />
            <meta name="author" content="PT Arajang Digital Indonesia" />
            <meta name="Version" content="v3.8.0" />
            <!-- favicon -->
            <link rel="shortcut icon" href="assets/images/favicon.ico">
            <!-- Font -->
            <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&display=swap" rel="stylesheet">
        </head>
        <body style="font-family: Nunito, sans-serif; font-size: 15px; font-weight: 400;">
    
            <!-- Hero Start -->
            <div style="margin-top: 50px;">
                <table cellpadding="0" cellspacing="0" style="font-family: Nunito, sans-serif; font-size: 15px; font-weight: 400; max-width: 600px; border: none; margin: 0 auto; border-radius: 6px; overflow: hidden; background-color: #fff; box-shadow: 0 0 3px rgba(60, 72, 88, 0.15);">
                    <thead>
                        <tr style="background-color: #F2467F; padding: 3px 0; border: none; line-height: 68px; text-align: center; color: #fff; font-size: 24px; letter-spacing: 1px;">
                            <th scope="col"><img src="'.base_url().'/assets/frontend/images/logo.png" height="32" alt=""></th>
                        </tr>
                    </thead>
        
                    <tbody>
                        <tr>
                            <td style="padding: 48px 24px 0; color: #161c2d; font-size: 18px; font-weight: 600;">
                                Hay, '.$name.'
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 15px 24px 15px; color: #8492a6;">
                            Terima kasih sudah mendaftar, Silahkan klik tombol dibawah ini untuk melakukan pembayaran :
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 15px 24px;">
                                <a href="'.$url.'" style="padding: 8px 20px; outline: none; text-decoration: none; font-size: 16px; letter-spacing: 0.5px; transition: all 0.3s; font-weight: 600; border-radius: 6px; background: linear-gradient(45deg, #F957A6, #F2467F) !important; color: #ffffff;">
                                Bayar Sekarang</a>
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 15px 24px 0; color: #8492a6;">
                            Tautan ini akan aktif selama 12 Jam sejak email ini dikirim.
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 15px 24px 15px; color: #8492a6;">
                                Monku Indonesia <br> Support Team
                            </td>
                        </tr>
        
                        <tr>
                            <td style="padding: 16px 8px; color: #8492a6; background-color: #f8f9fc; text-align: center;">
                                © <script>document.write(new Date().getFullYear())</script> Monku Indonesia.
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Hero End -->
        </body>
    </html>';

    email_config("Informasi Pembayaran Undangan Digital", $template, $to);
}

function email_config($subject, $message, $to, $attachment = null)
{
    $email = \Config\Services::email();

    $email->setFrom('cs@monku.id', 'Monku Indonesia');
    $email->setTo($to);

    $email->attach($attachment);

    $email->setSubject($subject);
    $email->setMessage($message);

    if (!$email->send()) {
        return $email->printDebugger();
        //return false;
    } else {
        return true;
    }
}
