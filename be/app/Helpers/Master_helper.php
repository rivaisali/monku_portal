<?php

use Config\App;
use App\Models\CustomModel;

function user($index)
{
    if (session()->has('user')) {
        $ret = session()->get("user")[$index];
        return $ret;
    } else {
        return 0;
    }
}

function invitation($index)
{
    if (session()->has('invitation')) {
        $ret = session()->get("invitation")[$index];
        return $ret;
    } else {
        return 0;
    }
}

function limits($index){
    $model = new CustomModel();
    $user_id = session()->get("user")["user_id"];
    $invitation_id = session()->get("invitation")["invitation_id"];
    $count = $model->select_data_count($index, "getRow", ["user_id" => $user_id, "invitation_id" => $invitation_id])->count;
    $limit = $model->select_data("rate_limits", "getRow", ["user_id" => $user_id, "invitation_id" => $invitation_id]);
    if($count >= $limit->$index) {
        return false;
    }else{
        return true;
    }
    //return $limit->$index;
}

function quota($index){
    $model = new CustomModel();
    $user_id = session()->get("user")["user_id"];
    $invitation_id = session()->get("invitation")["invitation_id"];
    $count = $model->select_data_count($index, "getRow", ["user_id" => $user_id, "invitation_id" => $invitation_id])->count;
    $quota = $model->select_data("rate_limits", "getRow", ["user_id" => $user_id, "invitation_id" => $invitation_id]);
    return $quota->$index - $count;
}

function rupiah($angka){
    $hasil_rupiah = number_format($angka,0,',','.');
    return $hasil_rupiah;
}

function generateGuestCode($length = 10)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return strtoupper($randomString);
}

